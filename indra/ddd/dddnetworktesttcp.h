#ifndef DD_NETWORK_TEST_TCP_H
#define DD_NETWORK_TEST_TCP_H
// 3D Avatar School 
// dddnetworktesttcp.h
//  network tests -  TCP 
// 

#include <string>
#include <vector>
#include "llpreprocessor.h"
#include "stdtypes.h"
#include "lltimer.h"
#include "dddnetworktestbase.h"

using std::string;
using std::vector;


class DDDNetworkTestTCP : public DDDNetworkTestBase 
{
public:
	DDDNetworkTestTCP();
	virtual ~DDDNetworkTestTCP();
	virtual void pump();

private:
	virtual void run();
	S32 socket_;
	LLTimer lastSent_;
};

#endif
