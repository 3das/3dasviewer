#ifndef DD_STATS_SENDER_H
#define DD_STATS_SENDER_H
// 3D Avatar School 
// dddstatssender.h
// Send machine stats to 3das servers
// 

#include <string>
using std::string;

class DDDStatsSender
{
public:
	static void sendMachineStats(string& slUser);

private:

};

#endif
