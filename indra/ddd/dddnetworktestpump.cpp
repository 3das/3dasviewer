

#include "dddnetworktestpump.h"
#include "dddnetworktestudp.h"
#include "dddnetworktesttcp.h"
//#include "dddnetworktesticmp.h"
#include "dddhttptransparentproxydetect.h"
#include "dddnetworktestroutertype.h"
#include "dddnetworktestlistexternalipstimer.h"


DDDNetworkTestPump::DDDNetworkTestPump()
{
	
}

void DDDNetworkTestPump::startup()
{
#ifdef LL_WINDOWS
	udp_ = new DDDNetworkTestUDP();
	tcp_ = new DDDNetworkTestTCP();
	//icmp_ = new DDDNetworkTestICMP();

	const char *vinit[] = {	"120.38.60.178", //oszz
							"176.34.52.164", // osjp (tools)
							"203.198.127.69", // oshk
							"58.67.156.153", // osgz aka tsgz
	};
	vector<string> targets(vinit, std::end(vinit));
	udp_->addTargets(targets);
	udp_->start();
	tcp_->addTargets(targets);
	tcp_->start();
	//icmp_->addTargets(targets);
	//icmp_->start();


	proxyDetect_ = new DDDHTTPTransparentProxyDetect();
	proxyDetect_-> start();

	routerType_ = new DDDNetworkTestRouterType();
	routerType_->start();

	externalIPTimer_ = new DDDNetworkTestListExternalIPsTimer();
	externalIPTimer_->start();	

#endif
}


