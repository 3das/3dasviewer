#ifndef DDD_NETWORK_TEST_LIST_EXTERNAL_IPS_TIMER_H
#define DDD_NETWORK_TEST_LIST_EXTERNAL_IPS_TIMER_H
// 3D Avatar School 
// dddnetworktestexternalipstimer.h
// timer and control for DDDNetworkListExternalIPs
// 
#include "llpreprocessor.h"
#include "stdtypes.h"
#include "llthread.h"

class DDDNetworkTestListExternalIPsTimer :  public LLThread
{
public:
	DDDNetworkTestListExternalIPsTimer();
private:
	virtual void run();
};

#endif

