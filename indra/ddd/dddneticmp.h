#ifndef DDD_NET_TEST_ICMP_H
#define DDD_NET_TEST_ICMP_H
// 3D Avatar School 
// dddneticmp.h
// platform independent ICMP code
// 


S32		ddd_start_net_icmp(S32& socket_out);
S32     ddd_send_ping (S32 socket, const char* target, int sequence);
S32     wait_ping_response (S32 socket, int& sequence_returned);



#endif
