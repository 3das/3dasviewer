#ifndef DDD_DEVICE_DRIVER_ENUMERATION_H
#define DDD_DEVICE_DRIVER_ENUMERATION_H
// 3D Avatar School 
// ddddevicedriverenumeration.h
// Enumerate device drivers
// 

#include <map>
#include "llsingleton.h"

class DDDDeviceDriverEnumeration : public LLSingleton<DDDDeviceDriverEnumeration>
{
public:
	DDDDeviceDriverEnumeration();
	void report();

};

#endif
