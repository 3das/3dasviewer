

#include <map>
#include "dddusbbusenum.h"
#include <iomanip>

#ifdef LL_WINDOWS
#include <windows.h>
#endif


DDDUsbBusEnum::DDDUsbBusEnum()
{

}

void DDDUsbBusEnum::logUSBDevices()
{
#ifdef LL_WINDOWS

    UINT numDevices = 0;
    GetRawInputDeviceList( NULL, &numDevices, sizeof( RAWINPUTDEVICELIST ) );
 
    // Got Any?
    if( !numDevices)
    {
		llinfos << "No USB numDevices found" << llendl;
		return;
    }
     
    PRAWINPUTDEVICELIST deviceListBuffer = new RAWINPUTDEVICELIST[ sizeof( RAWINPUTDEVICELIST ) * numDevices ];
 
    int rc = GetRawInputDeviceList( deviceListBuffer, &numDevices, sizeof( RAWINPUTDEVICELIST ) );
 
    if( rc < 0 )
    {
		llwarns << "Failed to allocate buffer for " << numDevices << " USB devices" << llendl;
        delete [] deviceListBuffer;
		return;
    }
 
	std::ostringstream str;
	str << "START USB DEVICE SUMMARY" << std::endl;
	str << "Type,Vendor:Product,Version" << std::endl;

    for( int i = 0; i < numDevices; i++ )
    {
 
        RID_DEVICE_INFO ridDeviceInfo;
        ridDeviceInfo.cbSize = sizeof( RID_DEVICE_INFO );
        UINT bufferSize = ridDeviceInfo.cbSize;
 
        rc = GetRawInputDeviceInfo( deviceListBuffer[i].hDevice,
                                         RIDI_DEVICEINFO,
                                         &ridDeviceInfo,
                                         &bufferSize );
 
        if( rc < 0 )
        {
			llwarns << "Failed to get device info for USB device " << i << llendl;
            continue;
        }
		str << ridDeviceInfo.dwType << "," 
			<< std::setw(4) << std::setfill('0') << std::hex << ridDeviceInfo.hid.dwVendorId << ":" 
			<< std::setw(4) << std::setfill('0') << std::hex << ridDeviceInfo.hid.dwProductId << "," 
			<< ridDeviceInfo.hid.dwVersionNumber << std::endl;
		
    }
	str << "END USB DEVICE SUMMARY" << std::endl;
	llinfos << str.str() << llendl;

 
    delete [] deviceListBuffer;

#endif

}



