

// dddgetworldserver.cpp
// Query 3DAS LMS for login URI

#include "dddgetworldserver.h"

#include "stdtypes.h"
#include "llpreprocessor.h"
#include "llthread.h"
#include "llprocessor.h"
#include "llpointer.h"
#include "llfile.h"
#include "llsingleton.h"
#include "llerror.h"

#include "../newview/llviewernetwork.h"
#include "../newview/llsecapi.h"
#include "llsys.h"
#include "../newview/llfeaturemanager.h"
#include "llgl.h"
#include "dddlogsender.h"
#include "../newview/llviewercontrol.h"
#include "../newview/llstartup.h"

#include "llhttpclient.h"
#include "../newview/llversioninfo.h"
#include "llbufferstream.h"
#include "lltrans.h"
#include "llnotifications.h"
#include "llnotificationsutil.h"

#include "dddsettings.h"

// Static data
string DDDGetWorldServer::loginUri_;


// from llstartup.cpp
extern bool login_alert_done(const LLSD& notification, const LLSD& response); 



class DDDGetWorldServerResponder : public LLHTTPClient::Responder
{
public:
	DDDGetWorldServerResponder(string& userName, void (*callback)(S32 option, void* user_data), void* userData) 
		: callback_(callback)
		, userData_(userData)
		, userName_(userName)
	{
	}

	virtual void error(U32 status, const string& reason);
	virtual void completedRaw(U32 status,
			const std::string& reason,
			const LLChannelDescriptors& channels,
			const LLIOPipe::buffer_ptr_t& buffer);


private:
	string findItem(const string& xmlFragment, const char* tag);
	void (*callback_)(S32 option, void *userdata);
	void* userData_;
	string userName_;
};


string DDDGetWorldServerResponder::findItem(const string& xmlFragment, const char* tag)
{
	size_t start=xmlFragment.find(tag) + strlen(tag) + 2; // after =" 
	return xmlFragment.substr(start, xmlFragment.find("\"", start) - start);
}

void DDDGetWorldServerResponder::completedRaw(U32 status,
			const std::string& reason,
			const LLChannelDescriptors& channels,
			const LLIOPipe::buffer_ptr_t& buffer)
{
	LLBufferStream istr(channels, buffer.get());
	std::stringstream strstrm;
	strstrm << istr.rdbuf();

	const std::string body = strstrm.str();
	
	if (body.length() == 0)
	{
		llwarns << "No reponse / timeout from LMS (" << DDDSettings::instance().getLMSBaseURI() << ")" << llendl;
		std::ostringstream emsg;
		emsg << LLNotifications::instance().getGlobalString("3dasLoginLMSTimeout") << "\n";

		LLSD args;
		args["ERROR_MESSAGE"] = emsg.str();
		LLNotificationsUtil::add("ErrorMessage", args, LLSD(), login_alert_done);
		LLStartUp::setStartupState(STATE_LOGIN_WAIT);
		return;

	}

	string statusCode = findItem(body, "status");
	string loginUri = findItem(body, "loginuri");
	string message = findItem(body, "message");
	string handoffApiUri = findItem(body, "handoffapiuri");

	llinfos << body << llendl;

	llinfos << "Status code: '" << statusCode << "' LoginURI: '" << loginUri 
			<< "' message: '" << message << "' handoffApiUri '" << handoffApiUri << "'" << llendl;
	
	if (atoi(statusCode.c_str()) == 200)
	{
		if (!loginUri.empty())
		{
			DDDGetWorldServer::setLoginURI(loginUri);
			if (!handoffApiUri.empty())
			{
				DDDSettings::instance().setBaseLMSURI(handoffApiUri);
				llinfos << "loginURI: '" << loginUri << "' LMS API: '" << handoffApiUri << "'" << llendl;
			}
			callback_(0, userData_);
			return;
		}
		else if (!handoffApiUri.empty())
		{
			// OK, not that LMS, it's telling us to try another one. 
			// But what if we're stuck in a loop . . . 
			if ( ! DDDSettings::instance().isLMSBaseURI(handoffApiUri))
			{
				// good, they are different! 
				DDDSettings::instance().setBaseLMSURI(handoffApiUri);
				DDDGetWorldServer::getWorldServer(userName_, callback_, userData_);
				return;
			}

			// this is sign of no class scheduled (Skype between Alistair McDonald and Patrick 
			// Kosterman 2012-08-18)
			llwarns << "LMS does not give loginURI but redirects to itself" 
				<< "last LMS URI: '" << DDDSettings::instance().getLMSBaseURI() 
				<< "' new returned handoff URI: '" << handoffApiUri << "'" << llendl;

			std::ostringstream emsg;
			emsg << LLNotifications::instance().getGlobalString("3dasNoClassFound") << "\n";

			LLSD args;
			args["ERROR_MESSAGE"] = emsg.str();
			LLNotificationsUtil::add("ErrorMessage", args, LLSD(), login_alert_done);
			LLStartUp::setStartupState(STATE_LOGIN_WAIT);
			return;
		}
		else
		{
			llwarns << "Cannot find details in message from LMS: Status code: '" << statusCode << "' LoginURI: '" << loginUri 
					<< "' message: '" << message << "' handoffApiUri '" << handoffApiUri << "'" << llendl;
			std::ostringstream emsg;
			emsg << LLNotifications::instance().getGlobalString("3dasGenericLoginProblem") << "\n";

			LLSD args;
			args["ERROR_MESSAGE"] = emsg.str();
			LLNotificationsUtil::add("ErrorMessage", args, LLSD(), login_alert_done);
			LLStartUp::setStartupState(STATE_LOGIN_WAIT);
			return;
		}
	}
	llwarns << "Drop-through behaviour - Status code: '" << statusCode << "' LoginURI: '" << loginUri 
			<< "' message: '" << message << "' handoffApiUri '" << handoffApiUri << "' LMS '" << DDDSettings::instance().getLMSBaseURI() << "'" << llendl;

	if (statusCode == "412") // user not found
	{
		std::ostringstream emsg;
		emsg << LLNotifications::instance().getGlobalString("3dasUserNotFoundInLMS") << "\n";

		LLSD args;
		args["ERROR_MESSAGE"] = emsg.str();
		LLNotificationsUtil::add("ErrorMessage", args, LLSD(), login_alert_done);
		LLStartUp::setStartupState(STATE_LOGIN_WAIT);
		return;
	}

	std::ostringstream emsg;
	emsg << LLNotifications::instance().getGlobalString("3dasGenericLoginProblem") << "\n";

	LLSD args;
	args["ERROR_MESSAGE"] = emsg.str();
	LLNotificationsUtil::add("ErrorMessage", args, LLSD(), login_alert_done);
	LLStartUp::setStartupState(STATE_LOGIN_WAIT);

}



void DDDGetWorldServerResponder::error(U32 status, const string& reason)
{
	llwarns << "GET failed; status: " << status << " reason: '" << reason << "' LMS '" << DDDSettings::instance().getLMSBaseURI() << "'" << llendl;
}




void DDDGetWorldServer::getWorldServer(string& slUser, void (*callback)(S32, void*), void* userData)
{
	lldebugs << "Entry" << llendl;

	if (slUser.find(" ") != string::npos) 
	{
		slUser[slUser.find(" ")] = '.';
	}

	LLSD cmdLineLoginUri = gSavedSettings.getLLSD("CmdLineLoginURI");
	lldebugs << cmdLineLoginUri << llendl;
	if (cmdLineLoginUri.isString())
	{
		// if there is a command-line parameter, don't query the LMS. Business as usual.
		callback(0,userData);
		return;
	}


#ifdef _WINDOWS_
// GRRR:
// for some reason the MS compiler says: 
// warning C4101: 'client' : unreferenced local variable
#pragma warning (disable: 4101)
#endif

	LLHTTPClient client;

	string LMSBaseURI = DDDSettings::instance().getLMSBaseURI();
	if (LMSBaseURI[LMSBaseURI.length()-1] != '/')
		LMSBaseURI += "/";
	string URI = LMSBaseURI + "getworldserver";

	LLSD headers;
	string userAgent = "3DAS Viewer version " + LLVersionInfo::getVersion();
	headers["User-Agent"] = userAgent.c_str();
	headers["Accept"] = "application/xml;*/*"; // or we get it gzipped. 
	headers["Host"] = DDDSettings::instance().getLMSHostname();

	LLSD  params;

	string enterprise = DDDSettings::instance().getEnterpriseCode();

	params["ent"] = enterprise;
	params["un"] = slUser;

	client.get(URI, params, new DDDGetWorldServerResponder(slUser, callback, userData), headers);

	lldebugs << "Leave" << llendl;

}
