#ifndef DD_NETWORK_TEST_PUMP_H
#define DD_NETWORK_TEST_PUMP_H
// 3D Avatar School 
// dddnetworktestpump.h
// pump class for network tests
// 

#include <map>
#include "llsingleton.h"

class DDDNetworkTestUDP;
class DDDNetworkTestTCP;
class DDDNetworkTestICMP;
class DDDHTTPTransparentProxyDetect;
class DDDNetworkTestRouterType;
class DDDNetworkTestListExternalIPsTimer;

class DDDNetworkTestPump :  public LLSingleton<DDDNetworkTestPump>
{
public:
	DDDNetworkTestPump();
	void startup();

private:
	DDDNetworkTestUDP* udp_;
	DDDNetworkTestTCP* tcp_;
	//DDDNetworkTestICMP* icmp_;
	DDDHTTPTransparentProxyDetect* proxyDetect_;
	DDDNetworkTestRouterType* routerType_;
	DDDNetworkTestListExternalIPsTimer* externalIPTimer_;

};

#endif
