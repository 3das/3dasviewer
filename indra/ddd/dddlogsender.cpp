// 3D Avatar School 
// dddlogsender.cpp
// send last logfile to server 
// 

#include "dddlogsender.h"
#include "llpreprocessor.h"
#include "llerror.h"
#include "llthread.h"
#include "stdtypes.h"
#include "llprocessor.h"
#include "llpointer.h"
#include "llfile.h"
#include "llsingleton.h"
#include "../newview/llversioninfo.h"

#ifdef _WINDOWS_
#include <Winbase.h>
#include <LMCons.h>
#endif

#include <iomanip>

#include "llsys.h"
#include "../newview/llsecapi.h"
#include "../newview/llviewernetwork.h"
#include "../newview/llviewercontrol.h"
#include "../newview/llloginhandler.h"			// gLoginHandler


#include "llhttpclient.h"
#include "llbase32.h"
#include "lldir.h"

#include "dddsettings.h"

#ifndef _WINDOWS_
#include <unistd.h>    // close
#include <stdlib.h>    // mkstemp
#include <pwd.h>       // for getOSUser
#include <sys/param.h>
#include <sys/stat.h>
#include <time.h>
#endif


class DDDLogSenderResponder : public LLHTTPClient::Responder
{
public:
	DDDLogSenderResponder() 
	{
		inError = false;
		fullySent = false;
	}

	virtual void error(U32 status, const std::string& reason)
	{
		llwarns << "POST failed; status: " << status << " reason: '" << reason << "'" << llendl;
		inError = true;
	}

	virtual void result(const LLSD& content)
	{	
		llinfos << "POST complete" << llendl;
		fullySent = true;
	}
private:
	bool inError;
	bool fullySent;
};



void DDDLogSender::sendLog(const string& logfileName)
{
	lldebugs << "Entry" << llendl;

	string prevSLUser = getPrevSLUser();
	string osUser = getOSUser();
	string osMachName = getOSMachName();
	string timestamp = getTimestampOfFile(gDirUtilp->getExpandedFilename(LL_PATH_LOGS, logfileName));
	if (timestamp == "Unknown") 
	{
		return; // file does not exist
	}

// GCC warning: use mkstemp instead of tmpnam
#ifdef _WINDOWS_
 	string tmpFile = gDirUtilp->getExpandedFilename(LL_PATH_TEMP, tmpnam(NULL));
#else
	string tmpTemplate = gDirUtilp->getExpandedFilename(LL_PATH_TEMP, "3dasClient-XXXXXX");
	char* templ = new char[tmpTemplate.length()];
	strcpy(templ, tmpTemplate.c_str());
	int fd = mkstemp(templ); 
	close(fd);
	string tmpFile = templ;
	delete templ;
#endif

	gzip_file(gDirUtilp->getExpandedFilename(LL_PATH_LOGS, logfileName), tmpFile);

#ifdef _WINDOWS_
	std::ifstream zippedFile(tmpFile, std::ios::binary);
#else
	std::ifstream zippedFile(tmpFile.c_str(), std::ios::binary);
#endif

	zippedFile.seekg (0, std::ios::end);
	size_t lengthZippedData = zippedFile.tellg();
	zippedFile.seekg (0, std::ios::beg);

	char* zipped_data = new char[lengthZippedData];
	zippedFile.read(zipped_data, lengthZippedData);
	zippedFile.close();
	remove(tmpFile.c_str());

	const std::string boundary = "----------------------------0123abcdefab";

	LLSD headers;
	headers["Content-Type"] = "multipart/form-data; boundary=" + boundary;
	string userAgent = "3DAS Viewer version " + LLVersionInfo::getVersion();
	headers["User-Agent"] = userAgent.c_str();

	std::ostringstream body;
	std::ostringstream tail;

	body	<< "--" << boundary << "\r\n"
			<< "Content-Disposition: form-data; name=\"prev_sluser\"\r\n\r\n"
			<< prevSLUser << "\r\n";

	body	<< "--" << boundary << "\r\n"
			<< "Content-Disposition: form-data; name=\"osuser\"\r\n\r\n"
			<< osUser << "\r\n";

	body	<< "--" << boundary << "\r\n"
			<< "Content-Disposition: form-data; name=\"osmachname\"\r\n\r\n"
			<< osMachName << "\r\n";

	body	<< "--" << boundary << "\r\n"
			<< "Content-Disposition: form-data; name=\"timestamp\"\r\n\r\n"
			<< timestamp << "\r\n";

	body	<< "--" << boundary << "\r\n"
			<< "Content-Disposition: form-data; name=\"logfile\"; ; filename=\"Secondlife.log\"\r\n"
		    << " Content-Type: application/octet-stream\r\n"
		    << " Content-Transfer-Encoding: binary\r\n\r\n";

	tail    << "\r\n";
    
	tail	<< "--" << boundary << "\r\n";

	size_t size = body.str().length() + lengthZippedData + tail.str().length();
	U8 *data = new U8[size];
	memcpy(data, body.str().c_str(), body.str().length());
	memcpy(data + body.str().length(), zipped_data, lengthZippedData);
	memcpy(data + body.str().length() + lengthZippedData, tail.str().c_str(), tail.str().length());

	delete[] zipped_data;

#ifdef _WINDOWS_
// GRRR:
// for some reason the MS compiler says: 
// warning C4101: 'client' : unreferenced local variable
#pragma warning (disable: 4101)
#endif

	LLHTTPClient client;

	string URI = DDDSettings::instance().getLogFileURI();	

	std::ostringstream contentLength;
	contentLength << size << std::ends;

	headers["Content-Length"] = contentLength.str().c_str();
	headers["Accept"] = "*/*";
	headers["Expect"] = "100-continue";
	headers["Host"] = DDDSettings::instance().getLMSHostname();

	client.postRaw(URI, data, size, new DDDLogSenderResponder(), headers);

	lldebugs << "Leave" << llendl;
	
}

#ifdef _WINDOWS_
string DDDLogSender::getOSUser()
{
	char szBuffer[UNLEN + 1];
	DWORD size = sizeof(szBuffer)/sizeof(szBuffer[0]);
	::GetUserNameA(szBuffer, &size);
	return string(szBuffer);
}

string DDDLogSender::getOSMachName()
{
	char szBuffer[MAX_COMPUTERNAME_LENGTH + 1];
	DWORD size=sizeof(szBuffer)/sizeof(szBuffer[0]);
	::GetComputerNameA(szBuffer, &size);
	return string(szBuffer);
}

#else
string DDDLogSender::getOSUser()
{
	register uid_t uid = geteuid();
	register struct passwd* pw = getpwuid(uid);

	return pw ? pw->pw_name : "UnknownUser";
}

string DDDLogSender::getOSMachName()
{
	char name[MAXHOSTNAMELEN];
	size_t namelen = MAXHOSTNAMELEN;

	if (gethostname(name, namelen) != -1)
		return name;
	
	return "UnknownMachine";
}

#endif


string DDDLogSender::getPrevSLUser()
{
	
	LLPointer<LLCredential> userCredential = gLoginHandler.initializeLoginInfo();

	if (userCredential.isNull())
	{
		return "Unknown";
	}

	LLSD identifier = userCredential.get()->getIdentifier();
	string slUser;
	if((std::string)identifier["type"] == "agent") 
	{
		slUser = "" + identifier["first_name"].asString() + "_" + identifier["last_name"].asString() + "";
	}
	else if (identifier["type"].asString() == "account")
	{
		slUser  = identifier["account_name"].asString();
	}
	else 
	{
		slUser = "Unknown";
	}
	return slUser;

}

string DDDLogSender::getTimestampOfFile(const string& logfileName)
{
#ifdef _WINDOWS_
	WIN32_FILE_ATTRIBUTE_DATA attrDetails;

	if (GetFileAttributesExA(logfileName.c_str(), GetFileExInfoStandard, &attrDetails))
	{
		SYSTEMTIME stUTC;
		FileTimeToSystemTime(&attrDetails.ftLastWriteTime, &stUTC);
		std::ostringstream timestamp;
		timestamp << std::setfill('0') << std::setw(4) << stUTC.wYear << "-" 
			<< std::setfill('0') << std::setw(2) << stUTC.wMonth<< "-" 
			<< std::setfill('0') << std::setw(2) << stUTC.wDay
			<< "T" 
			<< std::setfill('0') << std::setw(2) << stUTC.wHour << ":" 
			<< std::setfill('0') << std::setw(2) << stUTC.wMinute << ":" 
			<< std::setfill('0') << std::setw(2) << stUTC.wSecond
			<< "Z" << std::ends;
		{
			return timestamp.str().c_str();
		}
	}
#else
	struct stat sb;
	if (stat(logfileName.c_str(), &sb) != -1) {
		struct tm tmtime;
		if (localtime_r(&sb.st_mtime, &tmtime)) {
			char buf[80] = {0};
			if (strftime(buf, 79, "%Y-%m-%dT%H:%M:%SZ", &tmtime))
				return buf;
		}
	}

#endif

	return "Unknown";
}

