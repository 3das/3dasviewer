#include "dddhttptransparentproxydetect.h"

#include "llhttpclient.h"
#include "llbufferstream.h"
#include "../newview/llversioninfo.h"
#include "lltimer.h"
#include "llmd5.h"

#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>


// Linden respopnse class is delete()d after response is received, but we wait for response then act on data
// So, store anything useful in one of these, allocated by caller


struct HeaderChangeResponse
{
	bool done_;
	string returnedBody_;
	LLTimer timer_;
	HeaderChangeResponse() : done_(false){};
};

class DDDHttpHeaderChangeResponder : public LLHTTPClient::Responder
{
public:
	DDDHttpHeaderChangeResponder(HeaderChangeResponse& responseContainer) 
		: responseContainer_(responseContainer)
	{
	}

	virtual void error(U32 status, const std::string& reason)
	{
		llwarns << "POST failed; status: " << status << " reason: '" << reason << "'" << llendl;
	}

	virtual void completedRaw(U32 status,
			const std::string& reason,
			const LLChannelDescriptors& channels,
			const LLIOPipe::buffer_ptr_t& buffer);
private:
	HeaderChangeResponse& responseContainer_;
};




void DDDHttpHeaderChangeResponder::completedRaw(U32 status,
			const std::string& reason,
			const LLChannelDescriptors& channels,
			const LLIOPipe::buffer_ptr_t& buffer)
{
	responseContainer_.timer_.stop();
	LLBufferStream istr(channels, buffer.get());
	std::stringstream strstrm;
	strstrm << istr.rdbuf();

	responseContainer_.returnedBody_ = strstrm.str();
	responseContainer_.done_ = true;
}








// Linden respopnse class is delete()d after response is received, but we wait for response then act on data
// So, store anything useful in one of these, allocated by caller

struct CacheInconsistencyResponse
{
	bool done_;
	string checksumLastResponse_;
	CacheInconsistencyResponse() : done_(false){};
};


class DDDHttpCacheInconsistencyResponder : public LLHTTPClient::Responder
{
public:
	DDDHttpCacheInconsistencyResponder(CacheInconsistencyResponse& response) 
		:response_(response)
	{
	}

	virtual void error(U32 status, const std::string& reason)
	{
		llwarns << "POST failed; status: " << status << " reason: '" << reason << "'" << llendl;
	}

	virtual void completedRaw(U32 status,
			const std::string& reason,
			const LLChannelDescriptors& channels,
			const LLIOPipe::buffer_ptr_t& buffer);
private:
	CacheInconsistencyResponse& response_;
};




// Linden respopnse class is delete()d after response is received, but we wait for response then act on data
// So, store anything useful in one of these, allocated by caller

struct CacheInconsistencyInitResponse
{
	bool done_;
	string transactionId_;
	CacheInconsistencyInitResponse() : done_(false){};
};



void DDDHttpCacheInconsistencyResponder::completedRaw(U32 status,
			const std::string& reason,
			const LLChannelDescriptors& channels,
			const LLIOPipe::buffer_ptr_t& buffer)
{
	LLBufferStream istr(channels, buffer.get());




	LLBufferArray::segment_iterator_t segment = buffer.get()->beginSegment();
	int size = 0;

	while (segment != buffer.get()->endSegment())
	{
		size += (*segment).size();
		++segment;
	}


	std::auto_ptr<unsigned char> data(new unsigned char[size]);


	segment = buffer.get()->beginSegment();
	int pos = 0;


	while (segment != buffer.get()->endSegment())
	{
		memcpy(data.get()+pos, (*segment).data(), (*segment).size());	  /*Flawfinder: ignore*/
		pos += (*segment).size();
		++segment;
	}

	LLMD5 checksum;
	checksum.update( data.get(), size);
	checksum.finalize();

	char digest[33];
	checksum.hex_digest(digest);

	llwarns << digest << llendl;

	response_.checksumLastResponse_ = digest;

	response_.done_ = true;
}



class DDDHttpCacheConsistencySessionInitResponder : public LLHTTPClient::Responder
{
public:
	DDDHttpCacheConsistencySessionInitResponder(CacheInconsistencyInitResponse& response) 
		:response_(response)
	{
	}

	virtual void error(U32 status, const std::string& reason)
	{
		llwarns << "POST failed; status: " << status << " reason: '" << reason << "'" << llendl;
	}

	virtual void completedRaw(U32 status,
			const std::string& reason,
			const LLChannelDescriptors& channels,
			const LLIOPipe::buffer_ptr_t& buffer);
private:
	CacheInconsistencyInitResponse& response_;
};




void DDDHttpCacheConsistencySessionInitResponder::completedRaw(U32 status,
			const std::string& reason,
			const LLChannelDescriptors& channels,
			const LLIOPipe::buffer_ptr_t& buffer)
{
	LLBufferStream istr(channels, buffer.get());
	std::stringstream strstrm;
	strstrm << istr.rdbuf();

	// expect "Init Session:lt1v93i1gsmqv8d2mskojejjd7:"

	string& response = strstrm.str();

	size_t posFirstColon = response.find(":");
	size_t posLastColon = response.rfind(":");

	if ( posFirstColon == std::string::npos || ( posFirstColon == posLastColon))
	{
		// we expect two colons (OK, not checking for more than that, but...
		llwarns << "Invalid response '" << response << "' received" << llendl;
	}
	else
	{
		response_.transactionId_=response.substr(posFirstColon+1, posLastColon - posFirstColon -1);
	}

	response_.done_ = true;
}







DDDHTTPTransparentProxyDetect::DDDHTTPTransparentProxyDetect()
	: LLThread("DDDHTTPTransparentProxyDetect")
{
}


DDDHTTPTransparentProxyDetect::~DDDHTTPTransparentProxyDetect()
{
}


void DDDHTTPTransparentProxyDetect::run()
{
	checkForHeaderChange();
	checkForCacheInconsistencies();
}


string DDDHTTPTransparentProxyDetect::getSessionId()
{
#ifdef _WINDOWS_
// GRRR:
// for some reason the MS compiler says: 
// warning C4101: 'client' : unreferenced local variable
#pragma warning (disable: 4101)
#endif
	for (int i = 0; i < 5 ; ++i)
	{

		CacheInconsistencyInitResponse results;
			
		DDDHttpCacheConsistencySessionInitResponder* responder = new DDDHttpCacheConsistencySessionInitResponder(results);

		LLSD headers;
		string userAgent = "3DAS Viewer version " + LLVersionInfo::getVersion();
		headers["User-AgenT"] = userAgent.c_str();
		headers["Accept"] = "*/*";
		headers["Expect"] = "100-continue";

		LLSD  params;

		LLTimer timer;
		timer.start();
		timer.setTimerExpirySec(30);

		LLHTTPClient client;
		client.get("http://tools.3davatarschool.com/viewer_pd.php",  params, responder, headers);

		while (!timer.hasExpired() & !results.done_)
		{
			ms_sleep(500);
		}
		if (results.done_)
		{
			return results.transactionId_;
		}
		llwarns << "Timeout getting session ID, attempt " << i+1 << llendl;
	}
	return "";
}

void DDDHTTPTransparentProxyDetect::checkForCacheInconsistencies()
{
	string sessionID = getSessionId();
	if (sessionID.empty())
	{
		llwarns << "Failed to get session ID; tests skipped" << llendl;
		return;
	}

	std::stringstream strstrm;

	for (int i = 1; i < 5; ++i) // index into web scripts is not zero-based (my bad)
	{
		std::stringstream modeStr;
		modeStr << i;
		for (int j = 0; j < 2 ; j++) // this might get confusing
		{
			CacheInconsistencyResponse results;
			DDDHttpCacheInconsistencyResponder* responder = new DDDHttpCacheInconsistencyResponder(results);
			LLSD headers;
			string userAgent = "3DAS Viewer version " + LLVersionInfo::getVersion();
			headers["User-AgenT"] = userAgent.c_str();
			headers["Accept"] = "*/*";
			headers["Expect"] = "100-continue";

			LLTimer timer;
			timer.start();
			timer.setTimerExpirySec(30);

			LLSD  params;
			params["PHPSESSID"]=sessionID;
			params["m"]=modeStr.str();

			LLHTTPClient client;
			client.get("http://tools.3davatarschool.com/viewer_pd.php",  params, responder, headers);
			
			while (!timer.hasExpired() & !results.done_)
			{
				ms_sleep(500);
				if (j == 0 && results.checksumLastResponse_ != "a54f105d6a8f7e447be9b96a46adbc07")
				{
					strstrm << "Incorrect checksum '" << results.checksumLastResponse_ << "' in response to request " << j+1 << " for ID " << j << std::endl;
				}
			}
		}
	}
	if (strstrm.str().size())
	{
		string logMessage = "BEGIN HTTP CACHE INCONSISTENCY REPORT\n" + strstrm.str() + "\nEND HTTP CACHE INCONSISTENCY REPORT";
		llinfos << logMessage << llendl;
	}
}


void DDDHTTPTransparentProxyDetect::checkForHeaderChange()
{
	LLSD headers;
	string userAgent = "3DAS Viewer version " + LLVersionInfo::getVersion();
	headers["User-AgenT"] = userAgent.c_str();
	headers["Accept"] = "*/*";
	headers["Expect"] = "100-continue";

#ifdef _WINDOWS_
// GRRR:
// for some reason the MS compiler says: 
// warning C4101: 'client' : unreferenced local variable
#pragma warning (disable: 4101)
#endif
	HeaderChangeResponse response;
	LLHTTPClient client;
	string URI = "http://tools.3davatarschool.com/viewer_status.php";
	DDDHttpHeaderChangeResponder *responder =new DDDHttpHeaderChangeResponder(response);
	LLSD payload;
	response.timer_.start();
	client.get(URI, payload, responder, headers);

	while (!response.done_)
	{
		ms_sleep(500);
	}

// Host: tools.3davatarschool.com <br />
// Accept-Encoding: deflate, gzip <br />
// Accept: */* <br />
// Expect: 100-continue <br />
// User-AgenT: 3DAS Viewer version 1.0.7.0 <br />
// Client IP: 86.28.203.124 

	std::stringstream strstrm;
	F64 elapsed = response.timer_.getElapsedTimeF64();
	if (elapsed < 1.0)
	{
		strstrm << "Warning, suspect transparent proxy\n"
			"Results recieved in " << elapsed << " seconds\n"
			"should be at least one second" << std::endl;
	}

	string& body = response.returnedBody_;

	if (body.find("User-AgenT") == std::string::npos)
	{
		strstrm << "Warning, suspect transparent proxy\n"
			"Strange Caps header 'User-AgenT' not found in body '" << body << "'" << std::endl;
	}

	int numHeaders = 0;
	size_t pos = 0;
	while ((pos = body.find(": ", pos)) != std::string::npos)
	{
		numHeaders++;
		pos++; // else we find same occurence again
	}

	if (numHeaders > 6) 
	{
		strstrm << "Warning, suspect transparent proxy\n"
			"Extra headers recieved (should be nine) Headers received:\n"
				<< body << "\nEnd headers received" << std::endl;
	}
	
	if (strstrm.str().size())
	{
		string logMessage = "BEGIN TRANSPARENT PROXY REPORT\n" + strstrm.str() + "\nEND TRANSPARENT PROXY REPORT";
		llinfos << logMessage << llendl;
	}

}



