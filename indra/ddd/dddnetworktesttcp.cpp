// 3D Avatar School 
// dddnetworktesttcp.cpp
// TCP network tests to specifiec servers
// 

#include "dddnetworktesttcp.h"
#include "llpreprocessor.h"
#include "lldate.h"
#include "llhost.h"
#include "lliosocket.h"
#include "llerror.h"
#include "stdtypes.h"
#include "dddnettcp.h"
#include "dddnetworktestlistexternalips.h"

const int timeoutSeconds(15);


DDDNetworkTestTCP::DDDNetworkTestTCP()
{

}


void DDDNetworkTestTCP::run()
{
	for (vector<string>::const_iterator i = targets_.begin(); i != targets_.end(); ++i)
	{
		S32 socket;
		int nRes = ddd_start_net_conn(socket, (*i).c_str(), "50010");
		if (nRes)
		{
			llwarns << "ddd_start_net_conn failed for '" << (*i).c_str() << "'" << llendl;
		}
		else 
		{
			string message = "InitPingTest:" + (*i);
			LLTimer* timer = new LLTimer();
			ddd_send_packet(socket, message.c_str(), message.length());
			timer->start();
			timer->setTimerExpirySec(timeoutSeconds);
			details_[(*i)] = new SendResponseDetails(timer);

			if (ddd_set_sock_timeout(socket, timeoutSeconds*1000))
			{
				llwarns << "Failed to process '" << (*i) << "'; skipping" <<llendl;
			}
			else
			{
				char receiveBuffer[NET_BUFFER_SIZE];
				int size = ddd_receive_packet(socket, receiveBuffer);
				if (size == -1)
				{
					llwarns << "Error reading from socket" << llendl;
				}
				else if (size == 0)
				{
					details_[(*i)]->currentResponseState_ = SendResponseDetails::Timeout;
				}
				else
				{
					// decode ip address and stop appropriate timer // InitPingTest:58.67.156.153 
					receiveBuffer[size] = 0;
					string recievedPacket(receiveBuffer);
					if (recievedPacket.find("InitPingTest") != string::npos) // because service will respond to our second message
					{
						SendResponseDetails* details = details_[(*i)];
						F64 elapsed = details->timer_->getElapsedTimeF64();
						details->responseTime_ = elapsed;
						details->timer_->stop();
						details->timer_->reset();
						details->currentResponseState_ = SendResponseDetails::Good;
						std::ostringstream str;
						str << "PingResponseTime:" << elapsed;
						ddd_send_packet(socket, str.str().c_str(), str.str().length());

						string reportedIp = recievedPacket.substr(recievedPacket.rfind(";")+1,16); // 15? max size of dotted-quad
						string formattedLogReport = (*i) /* IP of server */ + "(" + reportedIp + ")";
						DDDNetworkTestListExternalIPs::instance().reportIP(DDDNetworkTestListExternalIPs::TCP , formattedLogReport);
					}
				}
			}
			ddd_close_socket(socket);
		}
	}
	DDDNetworkTestListExternalIPs::instance().finished(DDDNetworkTestListExternalIPs::TCP);

	// suppose could move to separate fn
	// summary time
	std::ostringstream str;
	
	str << "START TCP PERFORMANCE SUMMARY" << std::endl;
	str << "Host,response time (seconds)" << std::endl;
	for (vector<string>::const_iterator i = targets_.begin(); i != targets_.end(); ++i)
	{
		str << (*i) << ",";
		map<string, SendResponseDetails*>::const_iterator j = details_.find(*i);
		if (j != details_.end())
		{
			SendResponseDetails* item = (*j).second;
			switch (item->currentResponseState_)
			{
				case SendResponseDetails::Timeout:
					str << "Timeout";
					break;
				case SendResponseDetails::Good:
					str << item->responseTime_;
					break;
				default: // None
					break; // do nothing
			}
		}
		else
		{
				str << "N/A";
		}
		str << std::endl;
	}
	str << "END TCP PERFORMANCE SUMMARY" << std::endl;
	LL_INFOS("Network Performance") << str.str() << llendl;


}

void DDDNetworkTestTCP::pump()
{

}


DDDNetworkTestTCP::~DDDNetworkTestTCP()
{

}

