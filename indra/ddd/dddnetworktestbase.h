#ifndef DD_NETWORK_TEST_BASE_H
#define DD_NETWORK_TEST_BASE_H
// 3D Avatar School 
// dddnetworktestbase.h
// base class for network tests (UDP, TCP, ICMP)
// 

#include <string>
#include <vector>
#include <map>
#include "llpreprocessor.h"
#include "stdtypes.h"
#include "llthread.h"

using std::string;
using std::vector;
using std::map;
class LLTimer;

class DDDNetworkTestBase : public LLThread
{
public:
	DDDNetworkTestBase();
	virtual ~DDDNetworkTestBase();
	virtual void pump() = 0;
	void addTargets(const vector<string>& targets);
	

protected:
	struct SendResponseDetails
	{
		LLTimer* timer_;
		enum ResponseRXd {None, Good, Timeout};
		ResponseRXd currentResponseState_;
		F64 responseTime_;
		SendResponseDetails(LLTimer* t) :timer_(t), currentResponseState_(None), responseTime_(0) {}
	};
	vector<string> targets_;
	map<string, SendResponseDetails*> details_;

private:
	virtual void run();
};

#endif
