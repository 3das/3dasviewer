#ifndef DDD_NET_TEST_TCP_H
#define DDD_NET_TEST_TCP_H
// 3D Avatar School 
// dddnettesttcp.h
// platform independent TCP code
// 

#define NET_BUFFER_SIZE (0x2000) //as in llmessage/net.h


// Returns 0 on success, non-zero on error.
// Sets socket handler/descriptor, changes nPort if port requested is unavailable.
S32		ddd_start_net_conn(S32& socket_out, char const* recipient, char const* port);
void	ddd_end_net(S32& socket_out);
void    ddd_close_socket(S32 socket);

S32     ddd_set_sock_timeout(S32 socket, S32 timeout_ms);


// returns size of packet or -1 in case of error
S32		ddd_receive_packet(int hSocket, char * receiveBuffer);
BOOL	ddd_send_packet(int hSocket, const char *sendBuffer, int size);	// Returns TRUE on success.



#endif

