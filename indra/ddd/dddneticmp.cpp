
#if LL_WINDOWS
#define WIN32_LEAN_AND_MEAN
#include <winsock2.h>
#include <windows.h>
#include <ws2tcpip.h>
#endif

#include "stdtypes.h"
#include "dddneticmp.h"
#include "llpreprocessor.h"
#include "llerror.h"



// crazy internet checksum stuff
// http://www.erg.abdn.ac.uk/~gorry/course/inet-pages/ip-cksum.html
unsigned short ip_sum_calc(unsigned short len_ip_header, unsigned short buff[])
{
	unsigned long sum=0;

	for (int i=0;i<len_ip_header;i=i+2){
		unsigned short word16 =((buff[i]<<8)&0xFF00)+(buff[i+1]&0xFF);
		sum = sum + (unsigned long) word16; 
	}

	while (sum>>16)
		sum = (sum & 0xFFFF)+(sum >> 16);

	sum = ~sum;

	return ((unsigned short) sum);
}



#ifdef _MSC_VER
// The following two structures need to be packed tightly, but unlike
// Borland C++, Microsoft C++ does not do this by default.
#pragma pack(1)
#endif

#if LL_WINDOWS

// The IP header
struct IPHeader {
	BYTE h_len:4;           // Length of the header in dwords
	BYTE version:4;         // Version of IP
	BYTE tos;               // Type of service
	USHORT total_len;       // Length of the packet in dwords
	USHORT ident;           // unique identifier
	USHORT flags;           // Flags
	BYTE ttl;               // Time to live
	BYTE proto;             // Protocol number (TCP, UDP etc)
	USHORT checksum;        // IP checksum
	ULONG source_ip;
	ULONG dest_ip;
};

// ICMP header
struct ICMPHeader {
	BYTE type;          // ICMP packet type
	BYTE code;          // Type sub code
	USHORT checksum;
	USHORT id;
	USHORT seq;
	ULONG timestamp;    // not part of ICMP, but we need it
};

#ifdef _MSC_VER
#pragma pack()
#endif


const int TTL = 30;
const int MAX_PING_DATA_SIZE = 1024;
const int DEFAULT_PACKET_SIZE = 32;
const int MAX_PING_PACKET_SIZE = (MAX_PING_DATA_SIZE + sizeof(IPHeader));

const int PACKET_SIZE = max(sizeof(ICMPHeader),  min(MAX_PING_DATA_SIZE, (unsigned int)DEFAULT_PACKET_SIZE));

#endif




#if LL_WINDOWS

S32		ddd_start_net_icmp(S32& socket_out)
{
	// Start Winsock up
	WSAData wsaData;
	if (WSAStartup(MAKEWORD(2, 1), &wsaData) != 0) 
	{
		llwarns << "Windows Sockets initialization failed, err:" << WSAGetLastError() << llendl;
		WSACleanup();
		return 1;
	}


	// Create the socket
	SOCKET sd_ = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
	if (sd_ == INVALID_SOCKET) 
	{
		llwarns << "socket failed, err " << WSAGetLastError() << llendl;
           return 1;
	}
	

	if (setsockopt(sd_, IPPROTO_IP, IP_TTL, (const char*)&TTL, 
		sizeof(TTL)) == SOCKET_ERROR) 
	{
		llwarns << "Failed to set TTL, err:" << WSAGetLastError() << llendl;
		WSACleanup();
	}
	socket_out = sd_;
	return 0;

}

S32     ddd_send_ping (S32 socket, const char* target, int sequence)
{
	sockaddr_in dest;

	memset(&dest, 0, sizeof(dest));

	unsigned int addr = inet_addr(target);
	if (addr != INADDR_NONE) 
	{
		// It was a dotted quad number, so save result
		dest.sin_addr.s_addr = addr;
		dest.sin_family = AF_INET;
	}
	else {
		// Not in dotted quad form, so try and look it up
		hostent* hp = gethostbyname(target);
		if (hp != 0) 
		{
			// Found an address for that host, so save it
			memcpy(&(dest.sin_addr), hp->h_addr, hp->h_length);
			dest.sin_family = hp->h_addrtype;
		}
		else 
		{
			llwarns << "Failed to resolve destination ', " << target << "'" << llendl;
			return 1;
		}
	}
	ICMPHeader* send_buf = (ICMPHeader*)malloc(PACKET_SIZE);

	// Set up the packet's fields
	send_buf->type = 8; //ICMP_ECHO_REQUEST;
	send_buf->code = 0;
	send_buf->id = (USHORT)GetCurrentProcessId()+sequence;
	send_buf->seq = 0;
	send_buf->timestamp = 999;//GetTickCount();

	const unsigned long int filler = 0xDEADBEEF;
	char* datapart = (char*)send_buf + sizeof(ICMPHeader);
	int bytes_left = PACKET_SIZE - sizeof(ICMPHeader);
	while (bytes_left > 0) {
		memcpy(datapart, &filler, min(int(sizeof(filler)), 
			bytes_left));
		bytes_left -= sizeof(filler);
		datapart += sizeof(filler);
	}

	// Calculate a checksum on the result
	send_buf->checksum = ip_sum_calc(PACKET_SIZE, (unsigned short*)send_buf );

	int bwrote = sendto(socket, (char*)send_buf, PACKET_SIZE, 0, 
		(sockaddr*)&dest, sizeof(dest));
	if (bwrote == SOCKET_ERROR) {
		llwarns << "Failed to send icmp to destination ', " << target << "'" << llendl;
		return 1;
	}
	return 0;
}

S32     wait_ping_response (S32 socket, int& sequence_returned)
{
	sockaddr_in source;
	IPHeader* recv_buf = (IPHeader*)malloc(MAX_PING_PACKET_SIZE);
	// Receive replies until we either get a successful read,
	// or a fatal error occurs.

	// Wait for the ping reply
	int fromlen = sizeof(source);
	int bread = recvfrom(socket, (char*)recv_buf, 
		PACKET_SIZE + sizeof(IPHeader), 0,
		(sockaddr*)&source, &fromlen);
	if (bread == SOCKET_ERROR) {
		llwarns << "Failed to receive from socket " << llendl;
		return 1;
	}


	// Pull the sequence number out of the ICMP header.  If 
	// it's bad, we just complain, but otherwise we take 
	// off, because the read failed for some reason.
	unsigned short header_len = recv_buf->h_len * 4;
	ICMPHeader* icmphdr = (ICMPHeader*)
		((char*)recv_buf + header_len);
	sequence_returned = icmphdr->seq; 

	return 0;
}

#else

#endif
