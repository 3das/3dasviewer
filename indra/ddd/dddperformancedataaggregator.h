#ifndef 	DDD_PERFORMANCE_DATA_AGGREGATOR_H_
#define 	DDD_PERFORMANCE_DATA_AGGREGATOR_H_

#include "llpreprocessor.h"
#include "llsd.h"

#include "llsingleton.h"

class DDDPerformanceDataAggregator : public LLSingleton<DDDPerformanceDataAggregator>
{
public:
	DDDPerformanceDataAggregator(); // would have expected LLSingleton to allow this to be protected, but . . . 
	void accumulate(LLSD& perfData);
	void outputSummary();
private:
	struct TimerAggregate 
	{
		LLSD::Integer numCalls;
		LLSD::Real totalTime;
		TimerAggregate(){numCalls = 0; totalTime = 0;};
		void add(LLSD::Integer callsMade, LLSD::Real timeAdded);
	};
	std::map<LLSD::String, TimerAggregate> sTimers;
	int numTicks_;
	time_t lastPeriodicSummaryTime_;
	void initialise();
};



#endif

