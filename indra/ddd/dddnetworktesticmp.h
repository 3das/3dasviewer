#ifndef DD_NETWORK_TEST_ICMP_H
#define DD_NETWORK_TEST_ICMP_H
// 3D Avatar School 
// dddnetworktesticmp.h
//  network tests -  ICMP
// 

#include <string>
#include <vector>
#include "dddnetworktestbase.h"

using std::string;
using std::vector;


class DDDNetworkTestICMP : public DDDNetworkTestBase 
{
public:
	DDDNetworkTestICMP();
	virtual ~DDDNetworkTestICMP();
	virtual void pump();

private:
	virtual void run();
};

#endif
