#ifndef DD_LOG_SENDER_H
#define DD_LOG_SENDER_H
// 3D Avatar School 
// dddlogsender.h
// send last logfile to server 
// 

#include <string>

using std::string;


class DDDLogSender
{
public:
	static void sendLog(const string& logfileName);
	static string getOSUser(); //getusername
	static string getOSMachName(); // getcomputername
	static string getPrevSLUser(); 
	static string getTimestampOfFile(const string& logfileName);
};

#endif
