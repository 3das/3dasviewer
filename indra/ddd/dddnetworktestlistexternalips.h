#ifndef DDD_NETWORK_TEST_LIST_EXTERNAL_IPS_H
#define DDD_NETWORK_TEST_LIST_EXTERNAL_IPS_H
// 3D Avatar School 
// dddnetworktestlistexternalips.h
// collect and report viewer IPs as seen from various network tests
// 
#include <string>
#include <map>
using std::string;
using std::map;
#include "llsingleton.h"

class DDDNetworkTestListExternalIPs :  public LLSingleton<DDDNetworkTestListExternalIPs>
{
public:
	DDDNetworkTestListExternalIPs();
	enum Source {TCP, UDP, 
				 LAST};
	void reportIP(Source from, const string& report);
	void finished(Source from);
	bool isFinished() const;
	void reportToLog() const;
private:
	map<Source, string> responses_;
	map<Source, int> finishedFlags_;
};

#endif

