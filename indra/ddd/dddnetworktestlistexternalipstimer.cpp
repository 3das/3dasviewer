
#include "dddnetworktestlistexternalipstimer.h"
#include "dddnetworktestlistexternalips.h"
#include "lltimer.h"


DDDNetworkTestListExternalIPsTimer::DDDNetworkTestListExternalIPsTimer()
	: LLThread("DDDNetworkTestListExternalIPsTimer")
{

}

void DDDNetworkTestListExternalIPsTimer::run()
{
	LLTimer timeoutTimer;
	timeoutTimer.start();
	timeoutTimer.setTimerExpirySec(120);
	while (!timeoutTimer.hasExpired() && !DDDNetworkTestListExternalIPs::instance().isFinished())
	{
		ms_sleep(1000);
	}
	DDDNetworkTestListExternalIPs::instance().reportToLog();
}
