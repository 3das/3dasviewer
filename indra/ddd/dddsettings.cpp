#include "dddsettings.h"

#include "../newview/llviewercontrol.h"

struct enterpriseInfo 
{
	string shortCode;
	string longName;
	string LMSBaseURIDNS;
	string LMSBaseURIIP;
	string ViewerStartPageDNS;
	string ViewerStartPageIP;
	string AutoUpdateURIDNS;
};

enterpriseInfo enterpriseTable[] = 
{
	{
	 "default", 
	 "Default location", 
	 "http://localhost:9009/api/v1/",
	 "http://127.0.0.1:9009/api/v1/",
	 "http://localhost:9009/",
	 "http://127.0.0.1:9009/", 
	 "https://localhost/updater", 
	}
};

static const string defaultEnterprise("default");


class enterpriseInfoStore
{ 
public:
	enterpriseInfoStore();
	enterpriseInfo const* getInfoForShortCode(const string& shortCode) const;
private:
	map<string,enterpriseInfo> enterprises;
};


enterpriseInfoStore::enterpriseInfoStore()
{
	for (int i = 0; i < sizeof(enterpriseTable) / sizeof(enterpriseTable[0]); ++i)
	{
		enterprises[enterpriseTable[i].shortCode] = enterpriseTable[i];
	}
}



enterpriseInfo const* enterpriseInfoStore::getInfoForShortCode(const string& shortCode) const
{
	map<string,enterpriseInfo>::const_iterator pos = enterprises.find(shortCode);
	if (pos != enterprises.end())
	{
		return &(pos->second);
	}
	// Hmm, not good, but . . . 
	pos = enterprises.find(defaultEnterprise);
	if (pos != enterprises.end())
	{
		return &(pos->second);
	}	
	return 0;
}

static enterpriseInfoStore theStore;



DDDSettings::DDDSettings()
	: isTeacher_(FALSE)
	, DNSIsEnabled_(FALSE)
	, dieAfterStatsSend_(FALSE)
	, statsSent(FALSE)
	, dieCallbackCalled(FALSE)
	, noRenderFrames_(-1)
	, dddCacheEnabled(FALSE)
	, vfsEnabled(TRUE) 
{
	// nothing to see here folks, move along!
}


BOOL DDDSettings::isTeacher() const
{
	return isTeacher_;
}

BOOL DDDSettings::isDNSEnabled() const
{
	return DNSIsEnabled_;
}

BOOL DDDSettings::dieAfterStatsSend() const
{
	return dieAfterStatsSend_;
}


string DDDSettings::getEnterpriseCode() const
{
	if (enterpriseCode_.empty())
	{
		 enterpriseCode_ = gSavedSettings.getString("3dasEnterpriseAbbreviation");
		 if (enterpriseCode_.empty())
		{
			llwarns << "No Enterprise Name in config file" << llendl;
			enterpriseCode_ = defaultEnterprise;
		}
	}
	return enterpriseCode_;
}

string  DDDSettings::getLMSBaseURI() const
{
	if (lmsUri_.empty())
	{
		if (isDNSEnabled())
		{
			return getLMSBaseURIByName();
		}
		else 
		{
			return getLMSBaseURIByIP();
		}
	}
	if (lmsUri_ == "http://localhost:9009/api/v1/") // this is such a bodge, sorry everyone. 
	{
		llwarns << "hard-coded DNS solution" << llendl;
		return "http://127.0.0.1:9009/api/v1/";
	}
	return lmsUri_;
}

bool DDDSettings::isLMSBaseURI(const string& uri) const
{
	if (uri == "http://127.0.0.1:9009/api/v1" && lmsUri_ == "http://localhost:9009/api/v1/")
	{
		return true;
	}
	return uri == lmsUri_;
}


string  DDDSettings::getLMSHostname() const
{
	string baseURI;
	if (lmsUri_.empty())
		baseURI = getLMSBaseURIByName();
	else
		baseURI = lmsUri_; // if set on command-line, don't look up in table
	baseURI = baseURI.substr(strlen("http://"));
	size_t firstslash = baseURI.find("/");
	if (firstslash != std::string::npos)
	{
		string hostname = baseURI.substr(0, firstslash);
		return hostname;
	}
	return baseURI;
}

	
string  DDDSettings::getLMSBaseURIByName() const
{
	const enterpriseInfo* entInfo = theStore.getInfoForShortCode(getEnterpriseCode());
	if (entInfo)
		return entInfo->LMSBaseURIDNS;
	llwarns << "failed to find enterprise '" << getEnterpriseCode() <<"'" << llendl;
	return "";
}

string  DDDSettings::getLMSBaseURIByIP() const
{
	const enterpriseInfo* entInfo = theStore.getInfoForShortCode(getEnterpriseCode());
	if (entInfo)
		return entInfo->LMSBaseURIIP;
	llwarns << "failed to find enterprise '" << getEnterpriseCode() <<"'" << llendl;
	return "";
}




string  DDDSettings::getViewerStartPage() const
{
	LLSD viewerStartPage = gSavedSettings.getLLSD("viewerstart");
	llwarns << viewerStartPage << llendl;
	if (viewerStartPage.isString())
	{
		return gSavedSettings.getString("viewerstart");
	}
	if (isDNSEnabled())
	{
		return getViewerStartPageByName();
	}
	return getViewerStartPageByIP();
}

string  DDDSettings::getViewerStartPageByName() const
{
	const enterpriseInfo* entInfo = theStore.getInfoForShortCode(getEnterpriseCode());
	if (entInfo)
		return entInfo->ViewerStartPageDNS;
	llwarns << "failed to find enterprise '" << getEnterpriseCode() <<"'" << llendl;
	return "";
}

string  DDDSettings::getViewerStartPageByIP() const
{
	const enterpriseInfo* entInfo = theStore.getInfoForShortCode(getEnterpriseCode());
	if (entInfo)
		return entInfo->ViewerStartPageIP;
	llwarns << "failed to find enterprise '" << getEnterpriseCode() <<"'" << llendl;
	return "";
}






string  DDDSettings::getUpdaterServiceURI() const
{
 	return getUpdaterServiceURIByName();
}


string  DDDSettings::getUpdaterServiceURIByName() const
{
	const enterpriseInfo* entInfo = theStore.getInfoForShortCode(getEnterpriseCode());
	if (entInfo)
		return entInfo->AutoUpdateURIDNS;
	llwarns << "failed to find enterprise '" << getEnterpriseCode() <<"'" << llendl;
	return "";
}




string  DDDSettings::getLogFileURI() const
{
	string base = getLMSBaseURI();
	return base + "client/saveLog";
}



string  DDDSettings::getMachineStatsURI() const
{
	string base = getLMSBaseURI();
	return base + "client/saveStatistics";
}


BOOL DDDSettings::getNoRender() const
{
	return getNoRenderFrames() >= 0;
}

S32 DDDSettings::getNoRenderFrames() const
{
	return noRenderFrames_;
}


BOOL DDDSettings::isNavBarEnabled() const
{
	return getNoRender()||isTeacher();
}


BOOL DDDSettings::isDDDCacheEnabled() const
{
	return dddCacheEnabled;
}

BOOL DDDSettings::isVFSEnabled() const
{
	return vfsEnabled;
}



void DDDSettings::setTeacherFlag(BOOL isTeacher)
{
	isTeacher_ = isTeacher;
}

void DDDSettings::setDNSEnabled(BOOL dnsEnabled)
{
	DNSIsEnabled_ = dnsEnabled;
}

void DDDSettings::setfinishAfterStatsSend()
{
	dieAfterStatsSend_=TRUE;
}

void DDDSettings::setEnterpriseCode(const string& enterpriseCode)
{
	enterpriseCode_ = enterpriseCode;
}

void DDDSettings::setBaseLMSURI(const string& lmsUri)
{
	lmsUri_ = lmsUri;
	if (gSavedSettings.getString("3dasHandoffLMSURI") != lmsUri_)
	{
		llwarns << "Changing global LMS setting from '" << gSavedSettings.getString("3dasHandoffLMSURI") << "' to '" << lmsUri << "'" << llendl;
		gSavedSettings.setString("3dasHandoffLMSURI", lmsUri);
	}
}


void DDDSettings::setNoRenderFrames(S32 frames)
{
	noRenderFrames_ = frames;
}

void DDDSettings::setDDDCacheEnabled(BOOL enabled)
{
	dddCacheEnabled = enabled;
}

void DDDSettings::setVFSEnabled(BOOL enabled)
{
	vfsEnabled = enabled;
}
