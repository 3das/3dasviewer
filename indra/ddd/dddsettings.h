#ifndef DD_SETTINGS_H
#define DD_SETTINGS_H
// 3D Avatar School 
// dddsettings.h
// store of 3das specific settings 
// 

#include <string>
using std::string;
#include <map>
using std::map;

#include "llsingleton.h"

class LLSD;

class DDDSettings: public LLSingleton<DDDSettings>
{
public:
	DDDSettings(); // would have expected LLSingleton to allow this to be protected, but . . . 

	BOOL isTeacher() const;
	BOOL isDNSEnabled() const;
	BOOL dieAfterStatsSend() const;
	string getEnterpriseCode() const;
	string getLMSBaseURI() const;
	bool isLMSBaseURI(const string& uri) const;
	string getLMSHostname() const;
	string getViewerStartPage() const;
	string getUpdaterServiceURI() const;
	string getLogFileURI() const;
	string getMachineStatsURI() const;
	BOOL getNoRender() const;
	S32  getNoRenderFrames() const;
	BOOL isNavBarEnabled() const; // slurl bar
	
	BOOL isDDDCacheEnabled() const;
	BOOL isVFSEnabled() const;

	friend class LLAppViewer; // parses command-line options and sets some here
	friend class DDDGetWorldServerResponder; // LMS URI comes back in GetWorldServer response
	friend bool idle_startup(); // parses settings per account
	friend bool ddd_finish_after_stats_sent(const LLSD& notification, const LLSD& response);
	friend int DDDInitialize();


private:
	void setTeacherFlag(BOOL isTeacher);
	void setDNSEnabled(BOOL dnsEnabled);
	void setfinishAfterStatsSend();
	void setEnterpriseCode(const string& enterpriseCode);
	void setBaseLMSURI(const string& lmsUri);
	
	// disable rendering after this number of frames.
	// do not disable rendering if this value <= 0
	void setNoRenderFrames(S32 frames = 48);

	void setDDDCacheEnabled(BOOL enabled);
	void setVFSEnabled(BOOL enabled);

	string getLMSBaseURIByName() const;
	string getLMSBaseURIByIP() const;
	string getViewerStartPageByName() const;
	string getViewerStartPageByIP() const;
	string getUpdaterServiceURIByName() const;
	BOOL isTeacher_;
	BOOL DNSIsEnabled_;
	BOOL dieAfterStatsSend_;
	BOOL statsSent;
	BOOL dieCallbackCalled;
	mutable string enterpriseCode_;
	mutable string lmsUri_;

	S32 noRenderFrames_;
	BOOL dddCacheEnabled;						 
	BOOL vfsEnabled;
};

#endif
