// 3D Avatar School 
// dddnetworktestbase.cpp
// base class for network tests (UDP, TCP, ICMP)
// 

#include "dddnetworktestbase.h"


DDDNetworkTestBase:: DDDNetworkTestBase()
	: LLThread("DDDNetworkTestBase")
{

}

void DDDNetworkTestBase::addTargets(const vector<string>& targets)
{
	targets_ = targets;
}

void DDDNetworkTestBase::run()
{
	this->start();
}

DDDNetworkTestBase::~DDDNetworkTestBase()
{

}
