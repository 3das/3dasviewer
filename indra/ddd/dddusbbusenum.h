#ifndef DDD_USB_BUS_ENUM_H
#define DDD_USB_BUS_ENUM_H
// 3D Avatar School 
// dddusbbusenum.h
// USB bus enumeration stuff
// 

#include "llsingleton.h"

class DDDUsbBusEnum :  public LLSingleton<DDDUsbBusEnum>
{
public:
	DDDUsbBusEnum();
	void logUSBDevices();
};

#endif
