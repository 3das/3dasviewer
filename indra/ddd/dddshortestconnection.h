#ifndef DD_SHORTEST_CONNECTION_H
#define DD_SHORTEST_CONNECTION_H
// 3D Avatar School 
// dddshortestconnection.h
// Determine shortest connection to servers
// 

#include <string>
#include <vector>
#include "llsingleton.h"

using std::string;
using std::vector;


class DDDShortestConnection : public LLSingleton<DDDShortestConnection>
{
public:
	DDDShortestConnection();
	string getShortestConnection() const;

private:
	void init();
	vector<string> servers;

};

#endif
