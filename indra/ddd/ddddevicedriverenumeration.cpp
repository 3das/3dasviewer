#include "ddddevicedriverenumeration.h"

#ifdef LL_WINDOWS
#include <Windows.h>
#include <psapi.h>
#endif


#include <stdlib.h>
#include <string>
using std::string;
#include <iomanip>


DDDDeviceDriverEnumeration::DDDDeviceDriverEnumeration()
{

}


void DDDDeviceDriverEnumeration::report()
{
#ifdef LL_WINDOWS
	DWORD cbNeeded = 0;
	EnumDeviceDrivers (0, 0, &cbNeeded);
	int cDrivers = cbNeeded/sizeof(LPVOID);
	std::auto_ptr<LPVOID>drivers(new LPVOID[ cDrivers ]);


	if( EnumDeviceDrivers(drivers.get(), cbNeeded, &cbNeeded))
	{ 
		TCHAR szDriver[MAX_PATH];
#ifdef UNICODE
		const std::wstring realSystemRoot(_wgetenv(L"SystemRoot"));
#else
		const std::string realSystemRoot(getenv("SystemRoot"));
#endif

		std::stringstream strstrm;
		for (int i=0; i < cDrivers; i++ )
		{
			if(GetDeviceDriverFileName(drivers.get()[i], szDriver, sizeof(szDriver) /              sizeof(szDriver[0])))
			{
				std::wstring filename(szDriver);
				const std::wstring systemRootTag (L"\\SystemRoot");
				const size_t lenSysRootTag = wcslen(systemRootTag.c_str());
				if (filename.find(systemRootTag) == 0)
				{
					std::wstring::const_iterator start = filename.begin();
					std::wstring::const_iterator end = start;
					for (int j = 0 ; j < lenSysRootTag; ++j)
						++end;
					filename.replace(start, end, realSystemRoot);
				}

				std::auto_ptr<char> narrowFileNameCh(new char [filename.length()*2+1]);
				wcstombs(narrowFileNameCh.get(), szDriver, filename.length()*2+1);
				string narrowFileName(narrowFileNameCh.get());
				
			 
				DWORD handle = 0;

				DWORD verInfoSize = GetFileVersionInfoSize(filename.c_str(), &handle);
				std::auto_ptr<BYTE> versionInfo(new BYTE[verInfoSize]);
				if (!GetFileVersionInfo(filename.c_str(), handle, verInfoSize, versionInfo.get()))
				{
					strstrm << narrowFileName.c_str() << ",N/A";
				}
				else
				{
					// we have version information
					UINT    			len = 0;
					VS_FIXEDFILEINFO*   vsfi = NULL;
					VerQueryValue(versionInfo.get(), L"\\", (void**)&vsfi, &len);
					strstrm << narrowFileName << ",(" << HIWORD(vsfi->dwFileVersionMS) << "." <<
														LOWORD(vsfi->dwFileVersionMS) << "." <<
														HIWORD(vsfi->dwFileVersionLS)  << "." <<
														LOWORD(vsfi->dwFileVersionLS)  << ")";
				}

				HANDLE hFile = CreateFile(filename.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
				if (hFile)
				{
					FILETIME ftCreate;
					FILETIME ftAccess;
					FILETIME ftWrite;
					GetFileTime(hFile, &ftCreate, &ftAccess, &ftWrite);
					SYSTEMTIME stUTC;
					FileTimeToSystemTime(&ftWrite, &stUTC);
					//2012-11-29T15:27:56Z
					strstrm << "," << stUTC.wYear	
						<< "-"  << std::setfill('0') << std::setw(2) << stUTC.wMonth 
						<< "-"  << std::setfill('0') << std::setw(2) << stUTC.wDay
						<< "T" 
						<< std::setfill('0') << std::setw(2) << stUTC.wHour 
						<< ":" << std::setfill('0') << std::setw(2) << stUTC.wMinute 
						<< ":" << std::setfill('0') << std::setw(2) << stUTC.wSecond 
						<< "Z" << std::endl;
				}
				else 
				{
					strstrm << ",N/A" << std::endl;
				}
			}

		}
		string logMessage = "START DEVICE DRIVER SUMMARY\nFilename,version,timestamp(UCT)" + strstrm.str() + "\nEND DEVICE DRIVER SUMMARY";
		llinfos << logMessage << llendl;
	}
	else 
	{
        llwarns << "EnumDeviceDrivers failed" << llendl;
	}
   
#endif
}
