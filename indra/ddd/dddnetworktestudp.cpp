// 3D Avatar School 
// dddnetworktestudp.cpp
// UDP network tests to specifiec servers
// 

#include "dddnetworktestudp.h"
#include "llpreprocessor.h"
#include "lldate.h"
#include "llhost.h"
#include "lliosocket.h"
#include "llerror.h"
#include "net.h"
#include "dddnetworktestlistexternalips.h"

const int timeoutSeconds(15);


DDDNetworkTestUDP::DDDNetworkTestUDP()
{
	
}

void DDDNetworkTestUDP::runAgainstPort(int port)
{
	int portOnThisHost = NET_USE_OS_ASSIGNED_PORT;
	S32 result = start_net(socket_, portOnThisHost, NoBlock);
	if (result)
	{
		llwarns << "start_net failed" << llendl;
		return;
	}
	else
	{
		lastSent_.start();
		lastSent_.setTimerExpirySec(timeoutSeconds);
		for (vector<string>::const_iterator i = targets_.begin(); i != targets_.end(); ++i)
		{
			string message = "InitPingTest:" + (*i);
			LLTimer* timer = new LLTimer();
			send_packet(socket_, message.c_str(), message.length(), ip_string_to_u32((*i).c_str()), port);
			timer->start();
			details_[(*i)] = new SendResponseDetails(timer);
		}
	}

	// suppose could move to separate fn
	int numResponses = 0;
	bool earlyErrorExit = false;
	while (numResponses < targets_.size() && !lastSent_.hasExpired() && !earlyErrorExit)
	{
		char receiveBuffer[NET_BUFFER_SIZE];
		int size = receive_packet(socket_, receiveBuffer);
		if (size == -1)
		{
			llwarns << "Error reading from socket" << llendl;
			earlyErrorExit = true;
		}
		else if (size == 0)
		{
			// non-blocking, so expected. Sleep a little 
			micro_sleep(250); // those are micro-seconds
		}
		else
		{
			// decode ip address and stop appropriate timer // InitPingTest:58.67.156.153;86.28.203.124
			receiveBuffer[size] = 0;
			string recievedPacket(receiveBuffer);
			if (recievedPacket.find("InitPingTest") != string::npos) // because service will respond to our second message
			{
				size_t lastColon=recievedPacket.rfind(":")+1;
				size_t lastSemiColon=recievedPacket.rfind(";");
				string IP = recievedPacket.substr(lastColon, lastSemiColon == std::string::npos ? recievedPacket.length() - lastColon : lastSemiColon-lastColon);
				map<string, SendResponseDetails*>::const_iterator i = details_.find(IP);
				if (i != details_.end())
				{
					numResponses++;
					SendResponseDetails* details = (*i).second;
					F64 elapsed = details->timer_->getElapsedTimeF64();
					details->responseTime_ = elapsed;
					details->timer_->stop();
					details->timer_->reset();
					details->currentResponseState_ = SendResponseDetails::Good;
					std::ostringstream str;
					str << "PingResponseTime:" << elapsed;
					send_packet(socket_, str.str().c_str(), str.str().length(), ip_string_to_u32(IP.c_str()), 50010);
					string reportedIp = recievedPacket.substr(recievedPacket.rfind(";")+1,16); // 15? max size of dotted-quad
					string formattedLogReport = IP + "(" + reportedIp + ")";
					DDDNetworkTestListExternalIPs::instance().reportIP(DDDNetworkTestListExternalIPs::UDP, formattedLogReport);

				}
				else
				{
					llwarns << "Cannot determine host from response: '" << receiveBuffer << "' " << llendl;
					for (map<string, SendResponseDetails*>::const_iterator i = details_.begin() ; i != details_.end(); ++i)
					{
						llwarns << "Map is : ("  << (*i).first << "), (" << (*i).second << ")" << llendl;
					}
				}
			}
		}
	}
	// suppose could move to separate fn
	// summary time
	std::ostringstream str;
	
	str << "START UDP PERFORMANCE SUMMARY (port " << port << ")" << std::endl;
	str << "Host,response time (seconds)" << std::endl;
	for (map<string, SendResponseDetails*>::const_iterator i = details_.begin() ; i != details_.end(); ++i)
	{
		SendResponseDetails* item = (*i).second;
		str << (*i).first << ",";
		switch (item->currentResponseState_)
		{
		    case SendResponseDetails::None:
				str << "N/A";
				break;
		    case SendResponseDetails::Good:
				str << item->responseTime_;
			default: // None
				break; // do nothing
		}
		str << std::endl;
	}
	str << "END UDP PERFORMANCE SUMMARY" << std::endl;
	LL_INFOS("Network Performance") << str.str() << llendl;

}

void DDDNetworkTestUDP::run()
{
	for (int i = 5060; i <= 5062; ++i)
	{
		runAgainstPort(i);
	}
	runAgainstPort(12888);
	runAgainstPort(50010);
	DDDNetworkTestListExternalIPs::instance().finished(DDDNetworkTestListExternalIPs::UDP);

	
}

void DDDNetworkTestUDP::pump()
{

}


DDDNetworkTestUDP::~DDDNetworkTestUDP()
{

}

