#ifndef DD_GET_WORLD_SERVER_H
#define DD_GET_WORLD_SERVER_H
// 3D Avatar School 
// dddgetworldserver.h
// get login URI from 3DAS LMS
// 

#include <string>
using std::string;

#include "stdtypes.h"

class DDDGetWorldServer
{
public:
	friend class DDDGetWorldServerResponder;

	static void getWorldServer(string& slUser, void (*callback)(S32, void*), void* userdata);
	static string getLoginURI() {return loginUri_;};

private:
	static void setLoginURI(const string& newLoginUri){loginUri_ = newLoginUri;};
	static string loginUri_;
};

#endif
