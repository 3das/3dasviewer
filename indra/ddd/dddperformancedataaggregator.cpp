

#include "dddperformancedataaggregator.h"
#include "llerror.h"

#include <map>
#include <time.h>

const LLSD::String performanceItems[] = {
"Avatars",
"Fonts",
"Image Decode",
"Process Objects",
"Simple",
"Sort Draw State",
"Sort",
"UI",
"Update Avatar",
"Update UI",
"Volume",

"VFS Thread",

"DDDCache read",
"DDDCache write",
"VFS Cache read",
"VFS Cache write",



/*
"Add Chain",
"Add Scroll List Item",
"Adjust Timeout",
"Agent Network",
"Agent Position",
"Allocate",
"Alpha Objects",
"Alpha Shadow",
"Ares",
"Arrange",
"Atmospherics",
"Autopilot",
"Avatar Face",
"Avatar Shadow",
"Avatars",
"Balance Octree",
"Begin Query",
"Bind Array",
"Bind Buffer",
"Bind Deferred",
"Bind Indices",
"Binormal",
"Bloom",
"Build Floaters",
"Build Occlusion",
"Build Panels",
"Build",
"Bump Source Loaded",
"Bump Standard Callback",
"Bump",
"Calculate Interest",
"Calculate Notification Channel Region",
"Callback",
"Callbacks",
"Camera",
"Chain",
"Check Region Circuit",
"Class",
"Cleanup Drawable",
"Cleanup",
"Client Copy",
"Color",
"Create child widget",
"Create GL Normal Map",
"Create Object",
"Create XUI Children",
"Create",
"Cull Rebound",
"Date Format",
"Deferred Grass",
"Deferred Shading",
"Deferred Simple",
"Delete",
"Deref",
"Dirty Images",
"Do Update",
"Draw Water",
"Draw",
"Drawables",
"Dynamic Throttle",
"Emissive",
"Enable Sim",
"End Query",
"Entries Update",
"Face Geom",
"Faces",
"Fake VBO Update",
"Fetch",
"Filter Inventory",
"Find Edges",
"Find Widgets",
"First FBO",
"Floater Post Build",
"Focus First Item",
"Fonts",
"Frustum Culling",
"Fullbright",
"Fullscreen Lights",
"Gather",
"Generate Flexies",
"Generate Normal Map",
"Generate Normal",
"Generate Triangles",
"Generate Volumes",
"Geo Update",
"Geometry",
"Get Data",
"Get FolderViewItem by ID",
"Get Result",
"Glow Push",
"Glow",
"Grass VB",
"Grass",
"Handle Keyboard",
"Handle Mouse",
"Highlight Set",
"HTTP Header",
"HTTP Pipe",
"HTTP Responder",
"Hud Effects",
"HUD Effects",
"Idle Callbacks",
"Idle Copy",
"Idle Network",
"Idle",

"Index",
"Inventory List Refresh",
"Inventory Refresh",
"Inventory SD Deserialize",
"Inventory",
"Invisible",
"IO Sleep",
"LFS Thread",
"Light Set",
"List",
"LLSD to LLInitParam conversion",
"Load Avatar",
"Load Extern Floater Reference",
"Load Extern Panel Reference",
"Local Lights",
"Media",
"Memory Check",
"Message Acks",
"Min/Max",
"Misc",
"Moved List",
"Movelist",
"Network",
"Normal",
"Object Culling",
"Objects",
"Occlusion Early Fail",
"Occlusion State",
"Occlusion",
"Octree",
"Open and Select",
"Panel Construction",
"Panel PostBuild",
"Panel Setup",
"Particle VB",
"Pause Threads",
"Perform",
"Picking",
"Pipeline",
"Pools",
"Position",
"Post Sort",
"Post",
"Prioritize",
"Process Images",
"Process Messages",
"Process Objects",
"Projectors",
"Pump IO",
"Pump Respond",
"Pump",
"Push Occlusion",
"Readback Occlusion",
"Rebuild",
"Remove Drawable",
"Render Shadows",
"Render",
"RenderPool",
"Rescale",
"Reset Draw Order",
"Resize Screen Texture",
"Retexture",
"Retransmit",
"RGB to Luminance",
"S22XMLRPC Request",
"Sanitize Selection",
"SD2XMLRPC Response",
"SDRPC Client",
"SDRPC Response",
"SDRPC Server",
"Send Message",
"Server Socket",
"Service",
"Set Buffer",
"Set Subimage",
"Setup VAO",
"Shadow Map",
"Shadow Soften",
"Shiny",
"Simple Shadow",
"Simple",
"Simulate Particles",
"Skin",
"Sky Geometry",
"Sleep",
"Socket Reader",
"Socket Writer",
"Sort 2",
"Sort Draw State",
"Sort Drawables",
"Sort Inventory",
"Sort",
"Spare Idle",
"Spatial Partition",
"Stats",
"String Format",
"Swap",
"Syntax Coloring",
"Syntax Highlighting",
"System Messages",
"Terrain Shadow",
"Terrain VB",
"Terrain",
"Terse Rebuild",
"Test",
"Text Reflow",
"Texture Cache",
"Texture",
"Timeout Check",
"Timers",
"Trace",
"Translate string",
"Tree Shadow",
"Trees",
"UI String",
"UI",
"Unlink",
"Update Animation",
"Update Attachments",
"Update Audio",
"Update Avatar",
"Update Flexies",
"Update Grass",
"Update Hidden Anim",
"Update Hud",
"Update Images",
"Update Joints",
"Update LayoutStacks",
"Update LOD",
"Update Media",
"Update Move",
"Update Objectlist",
"Update Particles",
"Update Primitives",
"Update Region",
"Update Rigged",
"Update Sky",
"Update Terrain",
"Update Text Segments",
"Update Textures",
"Update Tree",
"Update UI",
"Update Water Params",
"Update Water",
"Update Windlight Params",
"Update World View",
"Update World",
"Update",
"Update/Interest",
"URL Complete",
"URL Configure",
"URL Extractor",
"URL Request",
"VBO Rebuilt",
"VFile Wait",
"VFS Thread",
"Viewer Object",
"Visible Cloud",
"Vivox Process",
"VL Manager",
"Volume Geometry",
"Volume Textures",
"Volume",
"Wait",
"Water",
"Weights",
"Widget Construction",
"Widget InitFromParams",
"Widget Setup",
"Windlight Sky",
"WL Param Update",
"XML Reading/Parsing",
"XMLRPC2LLSD Request",
"XMLRPC2LLSD Response",
"XUI Parsing",
*/
};

//static 


void DDDPerformanceDataAggregator::TimerAggregate::add(LLSD::Integer callsMade, LLSD::Real timeAdded)
{
	numCalls += callsMade;
	totalTime += timeAdded;
}

DDDPerformanceDataAggregator::DDDPerformanceDataAggregator()
	:numTicks_(0)
	,lastPeriodicSummaryTime_(time(NULL))
{
	initialise();
}

//static
void DDDPerformanceDataAggregator::initialise()
{
	for (int i = 0 ; i < sizeof(performanceItems) / sizeof(performanceItems[0]); ++i)
	{
		TimerAggregate t;
		sTimers[performanceItems[i]]=t;
	}
}


//static
void DDDPerformanceDataAggregator::accumulate(LLSD& sd)
{
	for (int i = 0 ; i < sizeof(performanceItems) / sizeof(performanceItems[0]); ++i)
	{
		LLSD& item = sd[performanceItems[i]];
		LLSD& calls = item["Calls"];
		LLSD& time = item["Time"];
		if (calls.isInteger() && time.isReal())
		{
			sTimers[performanceItems[i]].add(calls.asInteger(),time.asReal());
		}
	}
	numTicks_++;
	time_t now(time(NULL));
	if ((now - lastPeriodicSummaryTime_) > 300) // 5-min interval
	{
		outputSummary();
		lastPeriodicSummaryTime_ = now;
	}
}


// static
void DDDPerformanceDataAggregator::outputSummary()
{
	std::ostringstream str;
	
	str << "START PERFORMANCE SUMMARY" << std::endl;
	str << "Num Ticks sampled: " << numTicks_ << std::endl;
	str << "TIMER NAME: NumCalls, TotalTime" << std::endl;
	for (int i = 0 ; i < sizeof(performanceItems) / sizeof(performanceItems[0]); ++i)
	{
		DDDPerformanceDataAggregator::TimerAggregate& statsTotal =sTimers.find(performanceItems[i])->second;
		str << performanceItems[i] << ": " << statsTotal.numCalls << ", " << statsTotal.totalTime << std::endl;
	}
	str << "END PERFORMANCE SUMMARY" << std::endl;
	LL_INFOS("Performance") << str.str() << llendl;
	
}

