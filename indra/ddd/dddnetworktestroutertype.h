#ifndef DDD_NETWORK_TEST_ROUTER_TYPE_H
#define DDD_NETWORK_TEST_ROUTER_TYPE_H
// 3D Avatar School 
// dddnetworktestroutertype.h
// Determine customer's router type
// 
#include "llpreprocessor.h"
#include "stdtypes.h"
#include "llthread.h"
#include <vector>
#include <string>
using std::vector;
using std::string;

class DDDNetworkTestRouterType :  public LLThread
{
public:
	DDDNetworkTestRouterType();
private:
	virtual void run();
	void reportMACAddresses();
	void reportHttpRouters();
	vector<string> routers_;
};

#endif

