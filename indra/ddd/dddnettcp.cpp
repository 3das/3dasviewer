// 3D Avatar School 
// dddnetworktesttcp.cpp
// platform independent (?) TCP code
// 

#ifdef LL_WINDOWS
#define WIN32_LEAN_AND_MEAN
#include <winsock2.h>
#include <windows.h>
#include <ws2tcpip.h>
#endif

#include "stdtypes.h"
#include "dddnettcp.h"
#include "llpreprocessor.h"
#include "llerror.h"


#ifdef LL_WINDOWS

S32		ddd_start_net_conn(S32& socket_out, char const* recipient, char const* port)
{
	// Create socket
	// Init WinSock
	int nRet;
	int hSocket=0;
    WSADATA          stWSAData;

    struct addrinfo *results = NULL,
    *addrptr = NULL,
    hints;
 
	// Initialize windows specific stuff
	if (WSAStartup(0x0202, &stWSAData))
	{
		S32 err = WSAGetLastError();
		WSACleanup();
		llwarns << "Windows Sockets initialization failed, err " << err << llendl;
		return 1;
	}


    //
    // Resolve the server name
    //
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    nRet = getaddrinfo(
                        recipient,
                        port,
                        &hints,
                        &results
                        );
    if (nRet != 0)
    {
		llwarns << "getaddrinfo failed, err " << nRet << llendl;
		return nRet;
    }
    if (results == NULL)
    {
		llwarns <<"Failed to resolve host '" << recipient << "'" << llendl;
		return -1;
    }

    //
    // Walk through the list of addresses returned and connect to each one.
    //    Take the first successful connection.
    //
    addrptr = results;
    while (addrptr)
    {
        hSocket = socket(addrptr->ai_family, addrptr->ai_socktype, addrptr->ai_protocol);
        if (hSocket == INVALID_SOCKET)
        {
			llwarns << "socket failed, err " << WSAGetLastError() << llendl;
            return -1;
        }

        //
        // Notice that nothing in this code is specific to whether we 
        // are using UDP or TCP.
        // We achieve this by using a simple trick.
        //    When connect() is called on a datagram socket, it does not 
        //    actually establish the connection as a stream (TCP) socket
        //    would. Instead, TCP/IP establishes the remote half of the
        //    ( LocalIPAddress, LocalPort, RemoteIP, RemotePort) mapping.
        //    This enables us to use send() and recv() on datagram sockets,
        //    instead of recvfrom() and sendto()

		char hoststr[NI_MAXHOST];
		char servstr[NI_MAXSERV];

        nRet = getnameinfo( addrptr->ai_addr,
                            (socklen_t)addrptr->ai_addrlen,
                            hoststr,
                            NI_MAXHOST,
                            servstr,
                            NI_MAXSERV,
                            NI_NUMERICHOST | NI_NUMERICSERV
                            );
		if (nRet != 0)
		{
			llwarns << "getnameinfo failed, err " << nRet << llendl;
			return nRet;
		}

        nRet = connect(hSocket, addrptr->ai_addr, (int)addrptr->ai_addrlen);
        if (nRet == SOCKET_ERROR)
        {
            closesocket(hSocket);
            hSocket = INVALID_SOCKET;
            addrptr = addrptr->ai_next;
        }
        else
        {
            break;
        }
    }
	if (hSocket == INVALID_SOCKET)
	{
		llwarns  << "Failed to connect to host: '" << recipient <<"' port '" << port << "'" << llendl;
		return 1;
	}
	socket_out = hSocket;
    freeaddrinfo(results);
	return 0;
	
}


S32		ddd_receive_packet(int hSocket, char * receiveBuffer)
{
    int nRet = recv(hSocket, receiveBuffer, NET_BUFFER_SIZE, 0);
	if (nRet == SOCKET_ERROR)
	{
		llwarns << "receive failed, err " << WSAGetLastError() << llendl;
		return -1;
	}
	return nRet;
}



BOOL	ddd_send_packet(int hSocket, const char *sendBuffer, int size)
{
	int nRet = send(hSocket, sendBuffer, size, 0);
	if (nRet == SOCKET_ERROR)
	{
		llwarns << "send failed, err " << WSAGetLastError() << llendl;
		return FALSE;
	}
	return TRUE;
}


void ddd_close_socket(S32 socket)
{
	int nRet = closesocket(socket);
	if (nRet)
	{
		llwarns << "socket operation, err " << WSAGetLastError() << llendl;
	}
}


S32 ddd_set_sock_timeout(S32 socket, S32 timeout_ms)
{
	int nRet = setsockopt(socket, SOL_SOCKET, SO_RCVTIMEO, (const char*)&timeout_ms, sizeof(DWORD));//yuck, but read the API docs
	if (nRet)
	{
		llwarns << "socket operation, err " << WSAGetLastError() << llendl;
	}
	return nRet;
}

void	ddd_end_net(S32& socket_out)
{
	WSACleanup();
}


#else

S32		ddd_start_net_conn(S32& socket_out, char const* recipient, char const* port)
{
	return 0;
}
void	ddd_end_net(S32& socket_out)
{

}
void    ddd_close_socket(S32 socket)
{
}

S32 ddd_set_sock_timeout(S32 socket, S32 timeout_ms)
{
	return 0;
}

S32		ddd_receive_packet(int hSocket, char * receiveBuffer)
{
	return 0;
}
BOOL	ddd_send_packet(int hSocket, const char *sendBuffer, int size)
{
	return TRUE;
}



#endif
