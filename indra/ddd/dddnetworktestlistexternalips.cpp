
#include "dddnetworktestlistexternalips.h"




DDDNetworkTestListExternalIPs::DDDNetworkTestListExternalIPs()
{
	
}

void DDDNetworkTestListExternalIPs::reportIP(Source from, const string& result)
{
	std::map<Source, string>::iterator pos = responses_.find(from);
	if (pos == responses_.end())
	{
		responses_[from]=result;
	}
	else
	{
		(*pos).second += "," + result;
	}
}

void DDDNetworkTestListExternalIPs::finished(Source from)
{
	finishedFlags_[from]=1;
}

bool DDDNetworkTestListExternalIPs::isFinished() const
{
	return (finishedFlags_.size() == LAST);
}




void DDDNetworkTestListExternalIPs::reportToLog() const
{
	std::ostringstream str;
	
	str << "START EXTERNAL IP ADDRESS LIST SUMMARY" << std::endl;
	str << "PROTOCOL,OurServer(ReportedIP),OurServer(ReportedIP),..." << std::endl;
	for (map<Source, string>::const_iterator i = responses_.begin() ; i != responses_.end(); ++i)
	{
		switch ((*i).first)
		{
			case TCP:
				str << "TCP,";
				break;
			case UDP:
				str << "UDP,";
				break;
			default:
				str << "Unknown";
				break;
		}
		str << (*i).second << std::endl;
	}
	str << "END EXTERNAL IP ADDRESS LIST SUMMARY" << std::endl;
	LL_INFOS("Network Performance") << str.str() << llendl;
	
}
