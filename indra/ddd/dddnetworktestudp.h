#ifndef DD_NETWORK_TEST_USP_H
#define DD_NETWORK_TEST_USP_H
// 3D Avatar School 
// dddnetworktestudp.h
//  network tests -  UDP
// 

#include <string>
#include <vector>

#include "llpreprocessor.h"
#include "stdtypes.h"
#include "lltimer.h"

#include "dddnetworktestbase.h"

using std::string;
using std::vector;


class DDDNetworkTestUDP : public DDDNetworkTestBase
{
public:
	DDDNetworkTestUDP();
	virtual ~DDDNetworkTestUDP();
	virtual void pump();

private:
	virtual void run();
	void runAgainstPort(int port);
	S32 socket_;
	LLTimer lastSent_;
};

#endif
