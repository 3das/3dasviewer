// 3D Avatar School 
// dddnetworktesticmp.cpp
// ICMP network tests to specifiec servers
// 

#include "dddnetworktesticmp.h"
#include "llpreprocessor.h"
#include "lldate.h"
#include "llhost.h"
#include "lliosocket.h"
#include "llerror.h"
#include "stdtypes.h"
#include "dddneticmp.h"
#include "dddnettcp.h"

const int timeoutSeconds(15);


DDDNetworkTestICMP::DDDNetworkTestICMP()
{

}

void DDDNetworkTestICMP::run()
{
	int sequence = 1;
	for (vector<string>::const_iterator i = targets_.begin(); i != targets_.end(); ++i)
	{
		S32 socket;

		int nRes = ddd_start_net_icmp(socket);
		if (nRes)
		{
			llwarns << "ddd_start_net_icmp failed" << llendl;
			return;
		}
	
		if (ddd_set_sock_timeout(socket, timeoutSeconds*1000))
		{
			llwarns << "Failed to set socket timeout" << llendl;
			return;
		}


		LLTimer* timer = new LLTimer();
		details_[(*i)] = new SendResponseDetails(timer);
		nRes = ddd_send_ping(socket, (*i).c_str(), sequence);
		if (nRes)
		{
			llwarns << "Failed to send ping to '" << (*i).c_str() << "'" << llendl;
			details_[(*i)]->currentResponseState_ = SendResponseDetails::None;
		}
		else 
		{
			details_[(*i)]->timer_->start();
			int sequence_returned = -1;
			int nRet = wait_ping_response(socket, sequence_returned);
			details_[(*i)]->timer_->stop();
			details_[(*i)]->responseTime_ = details_[(*i)]->timer_->getElapsedTimeF64();

			if (nRet)
			{
				llwarns << "Failed to receive ping from '" << (*i).c_str() << "'" << llendl;
				details_[(*i)]->currentResponseState_ = SendResponseDetails::None;
			}

			if (sequence_returned == -1) // timeout
			{
				details_[(*i)]->currentResponseState_ = SendResponseDetails::Timeout;
			}
			else if (sequence_returned == sequence)
			{
				details_[(*i)]->currentResponseState_ = SendResponseDetails::Good;
			}
			else 
			{
				llwarns << "Unknown sequence number from '" << (*i).c_str() << "'" << llendl;
				details_[(*i)]->currentResponseState_ = SendResponseDetails::None;
			}
		}
		ddd_close_socket(socket);
		sequence++;
	}

	// suppose could move to separate fn
	// summary time
	std::ostringstream str;
	
	str << "START ICMP PERFORMANCE SUMMARY" << std::endl;
	str << "Host,response time (seconds)" << std::endl;
	for (vector<string>::const_iterator i = targets_.begin(); i != targets_.end(); ++i)
	{
		str << (*i) << ",";
		map<string, SendResponseDetails*>::const_iterator j = details_.find(*i);
		if (j != details_.end())
		{
			SendResponseDetails* item = (*j).second;
			switch (item->currentResponseState_)
			{
				case SendResponseDetails::Timeout:
					str << "Timeout";
					break;
				case SendResponseDetails::Good:
					str << item->responseTime_;
					break;
				default: // None
					break; // do nothing
			}
		}
		else
		{
				str << "N/A";
		}
		str << std::endl;
	}
	str << "END ICMP PERFORMANCE SUMMARY" << std::endl;
	LL_INFOS("Network Performance") << str.str() << llendl;

}

void DDDNetworkTestICMP::pump()
{

}

DDDNetworkTestICMP::~DDDNetworkTestICMP()
{

}

