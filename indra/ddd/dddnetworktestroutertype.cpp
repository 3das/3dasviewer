

#include <map>
#include "dddnetworktestroutertype.h"
#include "llhttpclient.h"
#include "llbufferstream.h"

#ifdef LL_WINDOWS
#include <Windows.h>
#include <iphlpapi.h>

#endif

using std::string;
using std::map;

DDDNetworkTestRouterType::DDDNetworkTestRouterType()
: LLThread("DDDNetworkTestRouterType"){
}


void DDDNetworkTestRouterType::run()
{
	reportMACAddresses();
	reportHttpRouters();
}

void DDDNetworkTestRouterType::reportMACAddresses()
{
#ifdef LL_WINDOWS


	// Stage one: Get routing table
	MIB_IPFORWARDTABLE ipForwardTable;
	DWORD tableSize = 0;

	// Get table size
	DWORD rc = GetIpForwardTable(&ipForwardTable, &tableSize, TRUE);
	if (rc != ERROR_INSUFFICIENT_BUFFER)
	{
		llwarns << "Failed to get routing table size; rc:" << rc << llendl;
		return;
	}

	// allocate space and get table
	std::auto_ptr<MIB_IPFORWARDTABLE> forwardTable(new MIB_IPFORWARDTABLE[tableSize]);
	rc = GetIpForwardTable(forwardTable.get(), &tableSize, TRUE);
	if (rc != NO_ERROR)
	{
		llwarns << "Failed to get routing table:" << rc << llendl;		
		return;
	}

	// MS Doc'n says anything with dwForwardDest == 0 is a default route, and there might be up to one per adapter
	map<string,u_long> potentialRouters;
    for (int i = 0; i < (int) forwardTable->dwNumEntries; i++) 
	{
		if (forwardTable->table[i].dwForwardDest == 0 )
		{
			in_addr IpAddr;
			IpAddr.S_un.S_addr = (u_long) forwardTable->table[i].dwForwardNextHop;
			potentialRouters[inet_ntoa(IpAddr)] = IpAddr.S_un.S_addr;
			routers_.push_back(inet_ntoa(IpAddr));
		}
	}

	if (potentialRouters.size()==0)
	{
		llwarns << "No default routes found in routing table " << llendl;
		return;
	}


	// Stage two: get ARP cache
	MIB_IPNETTABLE ipNetTable;
	tableSize = 0;

	rc = GetIpNetTable(&ipNetTable, &tableSize, TRUE);
	if (rc != ERROR_INSUFFICIENT_BUFFER)
	{
		llwarns << "Failed to get ARP table size; rc:" << rc << llendl;
		return;
	}

	// allocate space and get table
	std::auto_ptr<MIB_IPNETTABLE> arpTable(new MIB_IPNETTABLE[tableSize]);
	rc = GetIpNetTable(arpTable.get(), &tableSize, TRUE);
	if (rc != NO_ERROR)
	{
		llwarns << "Failed to get ARP table:" << rc << llendl;		
		return;
	}

	std::stringstream str;
	str << "START DEFAULT GATEWAY ARP SUMMARY" << std::endl;
	str << "Gateway,MAC" << std::endl;

	// stage 3: lookup  each default route in the ARP cache
	for (map<string,u_long>::const_iterator i = potentialRouters.begin(); i != potentialRouters.end(); ++i)
	{
		for (int j = 0; j < arpTable->dwNumEntries ; ++j)
		{
			if (arpTable->table[j].dwAddr == i->second) // a match
			{
				str << i->first << ",";
				for (int k = 0; k < 5 ; ++k)
					str << std::hex <<  std::setfill('0') << std::setw(2) << std::uppercase << (int)(arpTable->table[j].bPhysAddr[k]) << ":";
				str << std::hex << std::setfill('0') << std::setw(2) << std::uppercase  << (int)(arpTable->table[j].bPhysAddr[6]) << std::endl; // no colon after the last one
				continue;
			}
		}
	}
	str << "END DEFAULT GATEWAY ARP SUMMARY";
	llinfos << str.str() << llendl;

#endif
}



struct RouterDefaultPageResponse
{
	bool done_;
	string returnedBody_;
	RouterDefaultPageResponse() : done_(false){};
};


class DDDRouterResponder : public LLHTTPClient::Responder
{
public:
	DDDRouterResponder(RouterDefaultPageResponse& responseDetails)
		: results_(responseDetails)
	{
	}

	virtual void error(U32 status, const string& reason)
	{
		llinfos << "GET failed; status: " << status << " reason: '" << reason << "'" << llendl;
		results_.done_ = true;
	}
	virtual void completedRaw(U32 status,
			const std::string& reason,
			const LLChannelDescriptors& channels,
			const LLIOPipe::buffer_ptr_t& buffer);


private:
	RouterDefaultPageResponse& results_;
};

void DDDRouterResponder::completedRaw(U32 status,
			const std::string& reason,
			const LLChannelDescriptors& channels,
			const LLIOPipe::buffer_ptr_t& buffer)
{
	LLBufferStream istr(channels, buffer.get());
	std::stringstream strstrm;
	strstrm << istr.rdbuf();

	results_.returnedBody_ = strstrm.str();
	results_.done_ = true;
}








void DDDNetworkTestRouterType::reportHttpRouters()
{
	for (vector<string>::const_iterator i = routers_.begin() ; i != routers_.end(); ++i)
	{
#ifdef _WINDOWS_
// GRRR:
// for some reason the MS compiler says: 
// warning C4101: 'client' : unreferenced local variable
#pragma warning (disable: 4101)
#endif

		LLHTTPClient client;
		RouterDefaultPageResponse response;

		client.get((*i),  new DDDRouterResponder(response));
		while (!response.done_)
		{
			ms_sleep(500);
		}
		if (response.returnedBody_.size() == 0)
		{
			llinfos << "Router " << (*i) << " did not respond on port 80" << llendl;
		}
		else
		{
			llinfos << "Router " << (*i) << " served the following on port 80\n" << response.returnedBody_ << llendl;
		}
	}
}



