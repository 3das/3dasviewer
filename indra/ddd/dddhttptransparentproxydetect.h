#ifndef DDD_HTTP_TRANSPARENT_PROXY_DETECT_H_
#define DDD_HTTP_TRANSPARENT_PROXY_DETECT_H_

#include <string>
#include <vector>
#include <map>
#include "llpreprocessor.h"
#include "stdtypes.h"
#include "llthread.h"

using std::string;
using std::vector;
using std::map;
class LLTimer;



class DDDHTTPTransparentProxyDetect : public LLThread
{
public:
	DDDHTTPTransparentProxyDetect();
	virtual ~DDDHTTPTransparentProxyDetect();

private:
	virtual void run();
	void checkForHeaderChange();
	void checkForCacheInconsistencies();
	string getSessionId();

};



#endif

