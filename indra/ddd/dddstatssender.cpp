

// dddstatssender.cpp
// Send machine stats to 3das servers

#include "dddstatssender.h"

#include "stdtypes.h"
#include "llpreprocessor.h"
#include "llthread.h"
#include "llprocessor.h"
#include "llpointer.h"
#include "llfile.h"
#include "llsingleton.h"
#include "llerror.h"

#include "../newview/llviewernetwork.h"
#include "../newview/llsecapi.h"
#include "llsys.h"
#include "../newview/llfeaturemanager.h"
#include "llgl.h"
#include "dddlogsender.h"
#include "dddsettings.h"
#include "../newview/llviewercontrol.h"

#include "llhttpclient.h"
#include "../newview/llversioninfo.h"


class DDDStatsSenderResponder : public LLHTTPClient::Responder
{
public:
	DDDStatsSenderResponder() 
	{
		inError = false;
		fullySent = false;
	}

	virtual void error(U32 status, const string& reason)
	{
		llwarns << "POST failed; status: " << status << " reason: '" << reason << "'" << llendl;
		inError = true;
	}

	virtual void result(const LLSD& content)
	{	
		llinfos << "POST complete" << llendl;
		fullySent = true;
	}
private:
	bool inError;
	bool fullySent;
};


/*
POST parameters: cpu_type, cpu_speed, tot_phys_mem, tot_virt_mem,
avail_phys_mem, avail_virt_mem, gpu_model, gpu_mem, sluser, osuser,
osmachname, osname
*/

void DDDStatsSender::sendMachineStats(string& slUser)
{
	lldebugs << "Entry" << llendl;
	LLProcessorInfo p;
	string cpuName = p.getCPUBrandName();
	F64 cpuSpeed = p.getCPUFrequency();

	LLMemoryInfo l;
	U32 totalPhysicalMemory = l.getPhysicalMemoryKB();
	U32 availPhysicalMem;
	U32 availVirtualMem;
	l.getAvailableMemoryKB(availPhysicalMem, availVirtualMem);

	LLSD memInfo = l.getStatsMap();

	string totalvirtualMemory = memInfo.get("PageSize KB").asString();
	
	string graphicsCard = gGLManager.getRawGLString();
	U32 graphicsCardMem = gGLManager.mVRAM; // not _certain_ about this


	if (slUser == "")
	{
		slUser = DDDLogSender::getPrevSLUser();
	}

	if (slUser == "")
	{
		slUser = "Unknown";
	}

	string osUser = DDDLogSender::getOSUser();
	string osMachName = DDDLogSender::getOSMachName();
	LLOSInfo osInfo;
	string osIdString = osInfo.getOSString();


#ifdef _WINDOWS_
// GRRR:
// for some reason the MS compiler says: 
// warning C4101: 'client' : unreferenced local variable
#pragma warning (disable: 4101)
#endif

	LLHTTPClient client;

	string URI = DDDSettings::instance().getMachineStatsURI();	

	const string boundary = "----------------------------0123abcdefab";

	LLSD headers;
	headers["Content-Type"] = "multipart/form-data; boundary=" + boundary;
	string userAgent = "3DAS Viewer version " + LLVersionInfo::getVersion();
	headers["User-Agent"] = userAgent.c_str();
	headers["Host"] = DDDSettings::instance().getLMSHostname();

	std::ostringstream body;

	body	<< "--" << boundary << "\r\n"
			<< "Content-Disposition: form-data; name=\"cpu_type\"\r\n\r\n"
			<< cpuName << "\r\n";

	body	<< "--" << boundary << "\r\n"
			<< "Content-Disposition: form-data; name=\"cpu_speed\"\r\n\r\n"
			<< cpuSpeed << "\r\n";

	body	<< "--" << boundary << "\r\n"
			<< "Content-Disposition: form-data; name=\"tot_phys_mem\"\r\n\r\n"
			<< (LLSD::Integer)totalPhysicalMemory << "\r\n";

	body	<< "--" << boundary << "\r\n"
			<< "Content-Disposition: form-data; name=\"tot_virt_mem\"\r\n\r\n"
			<< totalvirtualMemory << "\r\n";

	body	<< "--" << boundary << "\r\n"
			<< "Content-Disposition: form-data; name=\"avail_phys_mem\"\r\n\r\n"
			<< (LLSD::Integer)availPhysicalMem << "\r\n";

	body	<< "--" << boundary << "\r\n"
			<< "Content-Disposition: form-data; name=\"avail_virt_mem\"\r\n\r\n"
			<< (LLSD::Integer)availVirtualMem << "\r\n";

	body	<< "--" << boundary << "\r\n"
			<< "Content-Disposition: form-data; name=\"gpu_model\"\r\n\r\n"
			<< graphicsCard << "\r\n";

	body	<< "--" << boundary << "\r\n"
			<< "Content-Disposition: form-data; name=\"gpu_mem\"\r\n\r\n"
			<< (LLSD::Integer)graphicsCardMem << "\r\n";

	body	<< "--" << boundary << "\r\n"
			<< "Content-Disposition: form-data; name=\"sluser\"\r\n\r\n"
			<< slUser << "\r\n";

	body	<< "--" << boundary << "\r\n"
			<< "Content-Disposition: form-data; name=\"osuser\"\r\n\r\n"
			<< osUser << "\r\n";

	body	<< "--" << boundary << "\r\n"
			<< "Content-Disposition: form-data; name=\"osmachname\"\r\n\r\n"
			<< osMachName << "\r\n";

	body	<< "--" << boundary << "\r\n"
			<< "Content-Disposition: form-data; name=\"osname\"\r\n\r\n"
			<< osIdString << "\r\n";

	body << std::ends;

	
	size_t size = body.str().length();
	U8 *data = new U8[size];
	memcpy(data, body.str().c_str(), size);

	std::ostringstream contentLength;
	contentLength << size << std::ends;

	headers["Content-Length"] = contentLength.str().c_str();
	headers["Accept"] = "*/*";
	headers["Expect"] = "100-continue";

	client.postRaw(URI, data, size, new DDDStatsSenderResponder(), headers);

	lldebugs << "Leave" << llendl;

}


