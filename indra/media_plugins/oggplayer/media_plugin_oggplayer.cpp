#include "linden_common.h"

#include "llgl.h"
#include "llplugininstance.h"
#include "llpluginmessage.h"
#include "llpluginmessageclasses.h"
#include "media_plugin_base.h"

#include <time.h>

#include "llcurl.h"
//#include "curl/curl.h"
#include "llbuffer.h"

#include "vorbis/codec.h"
#include "vorbis/vorbisfile.h"

#define LL_OPENAL 1

#ifdef LL_OPENAL
#include <AL/al.h>
#include <AL/alut.h>
#endif

#include <vector>
#include <dddcache.h>

//#include "lldir.h"

//#include "llaudioengine.h"
//#include "llaudioengine_openal.h"

//LLAudioEngine* g_audiop = (LLAudioEngine*) new LLAudioEngine_OpenAL();


////////////////////////////////////////////////////////////////////////////////
//
class MediaPluginOggPlayer: public MediaPluginBase
{
public:
	MediaPluginOggPlayer( LLPluginInstance::sendMessageFunction host_send_func, void *host_user_data );
	
	~MediaPluginOggPlayer();

	/*virtual*/ void receiveMessage( const char* message_string );

private:
	bool init();
	void update( double milliseconds );

	void load(const std::string& url);
	
	void curlLoad(const std::string& url);

	//int mCurVolume;
	//bool mMediaSizeChanging;
	bool mIsLooping;
	F64 mPlayRate;

	enum ECommand {
		COMMAND_NONE,
		COMMAND_STOP,
		COMMAND_PLAY,
		COMMAND_FAST_FORWARD,
		COMMAND_FAST_REWIND,
		COMMAND_PAUSE,
		COMMAND_SEEK,
	};
	ECommand mCommand;


public:
	// used in downCallback static method
	LLBufferArray mBufArray;
	//std::string mTmpOggFile;

private:
	std::vector<U8> mBufVect; // whole buffer

	DDDCache mOggCache;


	void play(F64 rate)
	{
		mPlayRate = rate;
		mCommand = COMMAND_PLAY;
	};

	void stop()
	{
		mCommand = COMMAND_STOP;
	};

	void pause()
	{
		mCommand = COMMAND_PAUSE;
	};


	void seek(F64 time)
	{
		//if ( mMovieController )
		//{
		//	TimeRecord when;
		//	when.scale = GetMovieTimeScale( mMovieHandle );
		//	when.base = 0;

		//	// 'time' is in (floating point) seconds.  The timebase time will be in 'units', where
		//	// there are 'scale' units per second.
		//	SInt64 raw_time = ( SInt64 )( time * (double)( when.scale ) );

		//	when.value.hi = ( SInt32 )( raw_time >> 32 );
		//	when.value.lo = ( SInt32 )( ( raw_time & 0x00000000FFFFFFFF ) );

		//	MCDoAction( mMovieController, mcActionGoToTime, &when );
		//};
	};


	void setVolume(F64 volume)
	{
	};


	void rewind()
	{
		// go to the beginning
	};



	static size_t downCallback(char* data, size_t size, size_t nmemb, void* userdata)
	{
		MediaPluginOggPlayer* pl = (MediaPluginOggPlayer*) userdata;

		U32 realsize = size * nmemb;

		//if (!ptrBufArray->count(0))
			//std::cout << "Resetting file" << std::endl;

		//std::ofstream of(pl->mTmpOggFile.c_str(), std::ios::binary | std::ios::out | std::ios::app );
		//of.write(data, realsize);

		pl->mBufArray.append(0, (U8*)data, realsize);

		return realsize;
	}
};



////////////////////////////////////////////////////////////////////////////////
//
MediaPluginOggPlayer::MediaPluginOggPlayer(LLPluginInstance::sendMessageFunction host_send_func, void *host_user_data)
 : MediaPluginBase(host_send_func, host_user_data),
   mOggCache("oggcache")
{
	//mWidth = 1024;  // needs to be non-zero if no later resize message is sent
	//mHeight = 1024; // needs to be non-zero if no later resize message is sent
	mDepth = 4;
	mPixels = 0;
	//mMouseButtonDown = false;
	//mStopAction = false;
	//mLastUpdateTime = 0;

	//mCurVolume = 0x99;
	//mMediaSizeChanging = false;
	//mIsLooping = false;
	//mPlayRate = 0.0f;
	
	mStatus = STATUS_NONE;
	mCommand = COMMAND_NONE;
}

////////////////////////////////////////////////////////////////////////////////
//
MediaPluginOggPlayer::~MediaPluginOggPlayer()
{
}



void MediaPluginOggPlayer::load(const std::string& url)
{
	if (url.empty())
		return;

	try {

		std::cout << "Fetching: " << url << std::endl;

		setStatus(STATUS_LOADING);

		//g_audiop->startInternetStream(url);

		//while(g_audiop->isInternetStreamPlaying() == LLAudioEngine::AUDIO_PLAYING)
			//Sleep(1000);

		// UUID is generated from the part of the URL after host name
		std::string hostName = "3davatarschool.com";
		std::string rest;
		auto hostIdx = url.find(hostName);
		if (hostIdx != std::string::npos)
		{
			auto restIdx = url.find("/", hostIdx);
			rest = url.substr(restIdx);
		}
		else
		{
			rest = url;
		}

		std::cout << "Generating UUID from: " << rest << std::endl;
		LLUUID uuid = LLUUID::generateNewID(rest);

		S32 fileSize = mOggCache.getSize(uuid);
		if (fileSize != -1)
		{
			std::cout << "Reading from OggCache " << fileSize << " bytes" << std::endl;

			mBufVect.clear();
			mBufVect.resize(fileSize);

			mOggCache.read(uuid, LLAssetType::AT_SOUND, mBufVect.data(), 0, mBufVect.size());
		}
		else
		{
			curlLoad(url);

			std::cout << "Fetched " << mBufArray.count(0) << " bytes" << std::endl;

			// NOTE: using LLCurlEasyRequest in this plugin causes APR network lib to close connection...
			//LLCurlEasyRequest curlRequest;
			//curlRequest.setopt(CURLOPT_NOSIGNAL, 1);
			//curlRequest.setWriteCallback(&downCallback, (void*) &mBufArray);
			//curlRequest.setopt(CURLOPT_HTTPGET, 1);
			//curlRequest.setopt(CURLOPT_FOLLOWLOCATION, 1);
			//curlRequest.setopt(CURLOPT_TRANSFERTEXT, 0);
	
			//curlRequest.sendRequest(url);
	
			//// sync
			//while(!curlRequest.isCompleted())
			//	curlRequest.wait();

			//CURLcode result;
			//LLCurl::TransferInfo transInfo;
			//if (curlRequest.getResult(&result, &transInfo))
			//{
			//		if (result != CURLE_OK)
			//		std::cout << "libcurl error: " << curlRequest.getErrorString() << std::endl;
			//		else
			//		std::cout << "Fetched " << transInfo.mSizeDownload << " bytes"<< std::endl;
			//}

			//std::cout << "outside Fetched " << transInfo.mSizeDownload << " bytes"<< std::endl;

			// fill the whole buffer
			mBufVect.clear();
			mBufVect.resize(mBufArray.count(0));
			S32 len = mBufVect.size();
			mBufArray.readAfter(0, NULL, mBufVect.data(), len);

			mOggCache.write(uuid, LLAssetType::AT_SOUND, mBufVect.data(), 0, mBufVect.size());
		}

		setStatus(STATUS_LOADED);

	} catch (...) {
		//std::cout << "exception"<< std::endl;
	}
}



void MediaPluginOggPlayer::curlLoad(const std::string& url)
{
	curl_global_init(CURL_GLOBAL_ALL);
 
	CURL* curlHandle = curl_easy_init();

	// cleans up stuff in destructor
	struct Finally
	{
		Finally(CURL* handle) : mHandle(handle) { }

		~Finally()
		{
			curl_easy_cleanup(mHandle);
			curl_global_cleanup();
		}
	
		CURL* mHandle;

	} dummyFinally(curlHandle);
 

	curl_easy_setopt(curlHandle, CURLOPT_URL, url.c_str());
	curl_easy_setopt(curlHandle, CURLOPT_WRITEFUNCTION, &downCallback);
	curl_easy_setopt(curlHandle, CURLOPT_WRITEDATA,  (void*) this);
	curl_easy_setopt(curlHandle, CURLOPT_USERAGENT, "libcurl-agent/1.0");
	curl_easy_setopt(curlHandle, CURLOPT_TRANSFERTEXT, 0); // binary mode
 
	CURLcode rc = curl_easy_perform(curlHandle);
	std::cout << "curl msg: \n" << curl_easy_strerror(rc) << std::endl;
 }



////////////////////////////////////////////////////////////////////////////////
//
void MediaPluginOggPlayer::receiveMessage( const char* message_string )
{
	//   std::cout << "entered MediaPluginOggPlayer::receiveMessage()" << std::endl;
	LLPluginMessage message_in;

	if ( message_in.parse( message_string ) >= 0 )
	{
		std::string message_class = message_in.getClass();
		std::string message_name = message_in.getName();

		if ( message_class == LLPLUGIN_MESSAGE_CLASS_BASE )
		{
			if ( message_name == "init" )
			{
				LLPluginMessage message( "base", "init_response" );
				LLSD versions = LLSD::emptyMap();
				versions[LLPLUGIN_MESSAGE_CLASS_BASE] = LLPLUGIN_MESSAGE_CLASS_BASE_VERSION;
				versions[LLPLUGIN_MESSAGE_CLASS_MEDIA] = LLPLUGIN_MESSAGE_CLASS_MEDIA_VERSION;
				// Normally a plugin would only specify one of these two subclasses, but this is a demo...
				versions[LLPLUGIN_MESSAGE_CLASS_MEDIA_TIME] = LLPLUGIN_MESSAGE_CLASS_MEDIA_TIME_VERSION;
				message.setValueLLSD("versions", versions);

				std::string plugin_version = "3DAS OGG Player media plugin, 3DAS OGG Player version 1.0.0.0";
				message.setValue( "plugin_version", plugin_version );
				sendMessage( message );
			}
			else if ( message_name == "idle" )
			{
				// no response is necessary here.
				double time = message_in.getValueReal( "time" );

				//std::cout << "MediaPluginOggPlayer::receiveMessage(): idle"<<std::endl;
				// Convert time to milliseconds for update()
				// (int)(time * 1000.0f)
				update( time );
			}
			else if ( message_name == "cleanup" )
			{
				// clean up here
			}
			else if ( message_name == "shm_added" )
			{
				SharedSegmentInfo info;
				info.mAddress = message_in.getValuePointer( "address" );
				info.mSize = ( size_t )message_in.getValueS32( "size" );
				std::string name = message_in.getValue( "name" );

				mSharedSegments.insert( SharedSegmentMap::value_type( name, info ) );

			}
			else if ( message_name == "shm_remove" )
			{
				std::string name = message_in.getValue( "name" );

				SharedSegmentMap::iterator iter = mSharedSegments.find( name );
				if( iter != mSharedSegments.end() )
				{
					if ( mPixels == iter->second.mAddress )
					{
						// This is the currently active pixel buffer.
						// Make sure we stop drawing to it.
						mPixels = NULL;
						mTextureSegmentName.clear();

						// Make sure the movie GWorld is no longer pointed at the shared segment.
						//sizeChanged();
					};
					mSharedSegments.erase( iter );
				}
				else
				{
					//std::cout << "MediaPluginOggPlayer::receiveMessage: unknown shared memory region!" << std::endl;
				};

				// Send the response so it can be cleaned up.
				LLPluginMessage message( "base", "shm_remove_response" );
				message.setValue( "name", name );
				sendMessage( message );
			}
			else
			{
				//std::cout << "MediaPluginOggPlayer::receiveMessage: unknown base message: " << message_name << std::endl;
			};
		}
		else if ( message_class == LLPLUGIN_MESSAGE_CLASS_MEDIA )
		{
			if(message_name == "init")
			{
				// This is the media init message -- all necessary data for initialization should have been received.

				// Plugin gets to decide the texture parameters to use.
				LLPluginMessage message(LLPLUGIN_MESSAGE_CLASS_MEDIA, "texture_params");
#if defined(LL_WINDOWS)
				// Values for Windows
				mDepth = 3;
				message.setValueU32("format", GL_RGB);
				message.setValueU32("type", GL_UNSIGNED_BYTE);

				// We really want to pad the texture width to a multiple of 32 bytes, but since we're using 3-byte pixels, it doesn't come out even.
				// Padding to a multiple of 3*32 guarantees it'll divide out properly.
				message.setValueU32("padding", 32 * 3);
#else
				// Values for Mac
				mDepth = 4;
				message.setValueU32("format", GL_BGRA_EXT);
#ifdef __BIG_ENDIAN__
				message.setValueU32("type", GL_UNSIGNED_INT_8_8_8_8_REV );
#else
				message.setValueU32("type", GL_UNSIGNED_INT_8_8_8_8);
#endif

				// Pad texture width to a multiple of 32 bytes, to line up with cache lines.
				message.setValueU32("padding", 32);
#endif
				message.setValueS32("depth", mDepth);
				message.setValueU32("internalformat", GL_RGB);
				message.setValueBoolean("coords_opengl", true);	// true == use OpenGL-style coordinates, false == (0,0) is upper left.
				message.setValueBoolean("allow_downsample", true);
				sendMessage(message);
			}
			else if ( message_name == "size_change" )
			{
				std::string name = message_in.getValue( "name" );
				S32 width = message_in.getValueS32( "width" );
				S32 height = message_in.getValueS32( "height" );
				S32 texture_width = message_in.getValueS32( "texture_width" );
				S32 texture_height = message_in.getValueS32( "texture_height" );

				if ( ! name.empty() )
				{
					// Find the shared memory region with this name
					SharedSegmentMap::iterator iter = mSharedSegments.find( name );
					if ( iter != mSharedSegments.end() )
					{
						mPixels = ( unsigned char* )iter->second.mAddress;
						mWidth = width;
						mHeight = height;

						mTextureWidth = texture_width;
						mTextureHeight = texture_height;

						//init();
					};
				};

				LLPluginMessage message( LLPLUGIN_MESSAGE_CLASS_MEDIA, "size_change_response" );
				message.setValue( "name", name );
				message.setValueS32( "width", width );
				message.setValueS32( "height", height );
				message.setValueS32( "texture_width", texture_width );
				message.setValueS32( "texture_height", texture_height );
				sendMessage( message );
			}
			else if ( message_name == "load_uri" )
			{
				std::string uri = message_in.getValue( "uri" );
				load(uri);
				//sendStatus();
			}
			else if ( message_name == "mouse_event" )
			{
				std::string event = message_in.getValue( "event" );
				S32 button = message_in.getValueS32( "button" );

				// left mouse button
				if ( button == 0 )
				{
					//int mouse_x = message_in.getValueS32( "x" );
					//int mouse_y = message_in.getValueS32( "y" );
					std::string modifiers = message_in.getValue( "modifiers" );

					if ( event == "move" )
					{
						//if ( mMouseButtonDown )
							//write_pixel( mouse_x, mouse_y, rand() % 0x80 + 0x80, rand() % 0x80 + 0x80, rand() % 0x80 + 0x80 );
					}
					else if ( event == "down" )
					{
						//mMouseButtonDown = true;
					}
					else if ( event == "up" )
					{
						//mMouseButtonDown = false;
					}
					else if ( event == "double_click" )
					{
					};
				};
			}
		}
		else if (message_class == LLPLUGIN_MESSAGE_CLASS_MEDIA_TIME)
		{
			if(message_name == "stop")
			{
				stop();
			}
			else if(message_name == "start")
			{
				F64 rate = 0.0;
				if(message_in.hasValue("rate"))
				{
					rate = message_in.getValueReal("rate");
				}
				play(rate);
			}
			else if(message_name == "pause")
			{
				pause();
			}
			else if(message_name == "seek")
			{
				F64 time = message_in.getValueReal("time");
				seek(time);
			}
			else if(message_name == "set_loop")
			{
				bool loop = message_in.getValueBoolean("loop");
				mIsLooping = loop;
			}
			else if(message_name == "set_volume")
			{
				F64 volume = message_in.getValueReal("volume");
				setVolume(volume);
			}
		}
		else
		{
			//std::cout << "MediaPluginOggPlayer::receiveMessage: unknown message class: " << message_class << std::endl;
		};
	};
}






namespace { // anon ns

	struct MemoryOggFile
	{
		char* curPtr;
		char* filePtr;
		size_t fileSize;
	};


	size_t mem_read_ogg(void* dst, size_t size, size_t nmemb, void* datasource)
	{
		MemoryOggFile* of = (MemoryOggFile*) datasource;

		size_t realsize = size * nmemb;

		if (of->curPtr + realsize > of->filePtr + of->fileSize)
		{
			realsize = of->filePtr + of->fileSize - of->curPtr;
		}

		memcpy(dst, of->curPtr, realsize);
		
		of->curPtr += realsize;

		return realsize;
	}


	int mem_seek_ogg(void* datasource, ogg_int64_t to, int type)
	{
		MemoryOggFile* of = (MemoryOggFile*) datasource;

		switch (type) {
		case SEEK_CUR:
			of->curPtr += to;
			break;

		case SEEK_END:
			of->curPtr = of->filePtr + of->fileSize - to;
			break;

		case SEEK_SET:
			of->curPtr = of->filePtr + to;
			break;

		default:
			return -1;
		}

		if (of->curPtr < of->filePtr) {
			of->curPtr = of->filePtr;
			return -1;
		}

		if (of->curPtr > of->filePtr + of->fileSize) {
			of->curPtr = of->filePtr + of->fileSize;
			return -1;
		}

		return 0;
	}


	int mem_close_ogg(void* datasource)
	{
		return 0;
	}

	long mem_tell_ogg(void *datasource)
	{
		MemoryOggFile* of = (MemoryOggFile*) datasource;

		return (of->curPtr - of->filePtr);
	}

} // anon ns


////////////////////////////////////////////////////////////////////////////////
//
void MediaPluginOggPlayer::update(double milliseconds)
{
	if (mCommand == COMMAND_PLAY)
	{
		setStatus(STATUS_PLAYING);
#ifdef LL_OPENAL
		std::cout << "Playing size: " << mBufVect.size() << std::endl;

		MemoryOggFile memOgg;
		memOgg.curPtr = memOgg.filePtr = (char*) mBufVect.data();
		memOgg.fileSize =  mBufVect.size();

		ov_callbacks callbacks;
		callbacks.read_func = mem_read_ogg;
		callbacks.seek_func = mem_seek_ogg;
		callbacks.close_func = mem_close_ogg;
		callbacks.tell_func = mem_tell_ogg;

		OggVorbis_File vf;
		
		//int rc = ov_fopen(mTmpOggFile.c_str(), &vf);
		int rc = ov_open_callbacks(&memOgg, &vf, NULL, 0, callbacks);
		//int rc = ov_open_callbacks((void*) &memOgg, &vf, (char*) mBufVect.get(), mBufSize, callbacks);
		if (rc)
		{
			std::cout << "Invalid Ogg bitstream. Error code: " << rc << std::endl;
		}
		else
		{
			std::cout << "Successfully open" << std::endl;
		}

		vorbis_info* vi = ov_info(&vf, -1);
		assert(vi);
		

		// OpenAL
		//ALint state;                // The state of the sound source
		ALuint bufferID;            // The OpenAL sound buffer ID
		ALuint sourceID;            // The OpenAL sound source
		ALenum format;              // The sound data format
		ALsizei freq;               // The frequency of the sound data
		std::vector<char> bufferData; // The sound buffer data from file

		freq = vi->rate;

		if (vi->channels == 1)
			format = AL_FORMAT_MONO16;
		else
			format = AL_FORMAT_STEREO16;

		int curSection;
		char pcmdata[32768]; // 32 KB

		while (long bytes = ov_read(&vf, pcmdata, sizeof(pcmdata), 0, 2, 1, &curSection))
		{
			if (bytes > 0)
			{
				 bufferData.insert(bufferData.end(), pcmdata, pcmdata + bytes);
			}
			else
			{
				std::cout << "Error reading Ogg bitsream" << std::endl;
				return;
			}
		}

		ov_clear(&vf);


		// init OpenAL
		alutInit(NULL, NULL);
		// Create sound buffer and source
		alGenBuffers(1, &bufferID);
		alGenSources(1, &sourceID);

		// Set the source and listener to the same location
		alListener3f(AL_POSITION, 0.0f, 0.0f, 0.0f);
		alSource3f(sourceID, AL_POSITION, 0.0f, 0.0f, 0.0f);

		// upload data to buffer
		alBufferData(bufferID, format, &bufferData[0], (ALsizei) bufferData.size(), freq);

		alSourcei(sourceID, AL_BUFFER, bufferID); // attach sound buffer to source
		
		alSourcePlay(sourceID); // play

		// wait
		ALint state;
		do {
			alGetSourcei(sourceID, AL_SOURCE_STATE, &state);
			// TODO: sleep some ms here
		} while (state != AL_STOPPED);

		
		alDeleteBuffers(1, &bufferID);
		alDeleteSources(1, &sourceID);

		alutExit();
#endif
		setStatus(STATUS_DONE);
		mCommand = COMMAND_NONE;
	}
};


////////////////////////////////////////////////////////////////////////////////
//
bool MediaPluginOggPlayer::init()
{
	LLPluginMessage message( LLPLUGIN_MESSAGE_CLASS_MEDIA, "name_text" );
	message.setValue( "name", "3DAS OGG Player Plugin" );
	sendMessage( message );

	return true;
};

////////////////////////////////////////////////////////////////////////////////
//
int init_media_plugin( LLPluginInstance::sendMessageFunction host_send_func,
	void* host_user_data,
	LLPluginInstance::sendMessageFunction *plugin_send_func,
	void **plugin_user_data )
{
	MediaPluginOggPlayer* self = new MediaPluginOggPlayer( host_send_func, host_user_data );
	*plugin_send_func = MediaPluginOggPlayer::staticReceiveMessage;
	*plugin_user_data = ( void* )self;

	return 0;
}