# -*- cmake -*-

include (LLRender)
include (LLVFS)

set(DDD_INCLUDE_DIRS
    ${LIBS_OPEN_DIR}/ddd
    ${LIBS_OPEN_DIR}/llvfs
    )

set(DDD_LIBRARIES ddd)