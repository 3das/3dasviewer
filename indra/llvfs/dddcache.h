#ifndef DDD_CACHE_H
#define DDD_CACHE_H

// 3D Avatar School 
// File based caching for VFS replacement. Currently used along with it

#include <map>
#include <set>
#include <string>
#include <memory>
#include <unordered_map>

#ifdef DDD_STANDALONE
#include <iostream>

#define llinfos (std::cout << "Info: ")
#define llwarns (std::cout << "Warning: ")
#define llerrs (std::cerr << "Error: ")

#define llendl (std::endl)


// define stubs for LLMutex and LLMutexLock

struct LLMutex {
	LLMutex(void*) {}
};

struct LLMutexLock {
	LLMutexLock(const LLMutex*) {}
};

#endif


#ifdef DDD_STANDALONE
#define DDD_USE_BOOST	// boost fs or apache portable runtime fs
#endif


#ifdef DDD_STANDALONE
#include "slmisc.h"
#include "lluuid_mini.h"
#else
#include "lluuid.h"
#include "llthread.h"
#include "llassettype.h"
#endif

// explicitly set the maximum thread safe mode - serialization
//#define SQLITE_THREADSAFE 1 
#include "SQLiteC++/Database.h"
#include "SQLiteC++/Statement.h"


#ifdef DDD_USE_BOOST
typedef void LLVolatileAPRPool;  // stub
#else
class LLAPRFile;
class LLVolatileAPRPool;
#endif



class DDDCache {
public:
	
	// dir where dddcache dir will be created
	// by default 3dasViewer cache dir or the current directory in standalone mode
	DDDCache(const std::string& cacheName = "dddcache", const std::string& parentDir = "");

	~DDDCache();


	S32 getSize(const LLUUID& uuid);

	BOOL exists(const LLUUID& uuid);

	// retrieve data by uuid
	// size - size of preallocated `data`, which might be more than actual asset file size 
	// location is always >= 0
	S32 read(const LLUUID& uuid, LLAssetType::EType assetType, U8* data, S32 location, S32 size);

	// write data to the cache. location -1 means append
	S32 write(const LLUUID& uuid, LLAssetType::EType assetType, const U8* data, S32 location, S32 size);

	void renameFile(const LLUUID& uuid, LLAssetType::EType assetType, const LLUUID& uuidNew, LLAssetType::EType assetTypeNew);

	void removeFile(const LLUUID& uuid, LLAssetType::EType assetType);
	
private:
	void purgeFilesIfNeeded();

	S32 readMemData(const std::vector<U8>& vect, U8* data, S32 location, S32 size) const;

private:

	struct CacheEntry
	{
		CacheEntry() : mID(-1), mFileSize(0), mAssetType(LLAssetType::AT_NONE), mTime(0) {}

		CacheEntry(S64 id, const LLUUID& uuid, const std::string& hash, S32 fileSize, LLAssetType::EType assetType, U32 time) :
					mID(id), mUUID(uuid), mHash(hash), mFileSize(fileSize), mAssetType(assetType), mTime(time) {}
	

		CacheEntry& operator=(const CacheEntry& entry)
		{	
			mID = entry.mID;
			mUUID = entry.mUUID;
			mHash = entry.mHash;
			mFileSize = entry.mFileSize;
			mAssetType = entry.mAssetType;
			mTime = entry.mTime;

			return *this;
		}
	
		S64 mID;			// sqlite db id
		LLUUID mUUID;		// 16 bytes
		std::string mHash;	// md5 hash
		S32 mFileSize;		// size of the corresponding file
		LLAssetType::EType mAssetType;		// texture, ogg, etc...
		U32 mTime;			// seconds since 1/1/1970
	};

		
	// clears cache and recreates dir structure. called when cache is corrupted
	void resetCache();

	std::string getDataFilePath(const LLUUID& uuid) const;

//#ifndef DDD_USE_BOOST
	LLVolatileAPRPool* getLocalAPRFilePool() { return mLocalAPRFilePoolp; }
//#endif

	// SQLite related methods ----------
	
	// create entries table if needed, calls dbPrepareStmts, leaves mDB connection open
	// retryOnce in case of failure
	bool dbInit(bool retryOnce);

	// prepare statements
	bool dbPrepareStmts();

	// -1 on error
	S32 dbGetSize(const LLUUID& uuid);

	// finds by uuid. returns false and sets entry.mID to -1 if not found
	bool dbFindEntry(const LLUUID& uuid, CacheEntry& entry);

	// shorthand for updating only timestamp. returns false if not found. finds by entry.mID
	bool dbUpdateEntryTime(const CacheEntry& entry);

	// inserts new record and assigns id to entry.mID
	void dbInsertEntry(CacheEntry& entry);

	// updates existing record. finds by entry.mID
	void dbUpdateEntry(const CacheEntry& entry);

	void dbRenameEntry(const LLUUID& uuid, LLAssetType::EType assetType, const LLUUID& uuidNew, LLAssetType::EType assetTypeNew);

	void dbRemoveEntry(const LLUUID& uuid, LLAssetType::EType assetType);

	// frees statements and closes db
	void dbClose();

private:

	std::string mCacheDirPath;  // dddcache
	std::string mEntriesDbPath; // dddcache.sdb

//#ifndef DDD_USE_BOOST
	LLVolatileAPRPool* mLocalAPRFilePoolp;
//#endif

	LLMutex mDBMutex;

	int mPurgeCheckCounter; // check for purging only once in a while

	// since destruction happens in reverse order, mDB should be
	// declared first and statements should follow afterwards
	std::unique_ptr<SQLite::Database> mDB;
	std::unique_ptr<SQLite::Statement> mStmtGetSize;
	std::unique_ptr<SQLite::Statement> mStmtFindEntry;
	std::unique_ptr<SQLite::Statement> mStmtInsertEntry;
	std::unique_ptr<SQLite::Statement> mStmtUpdateEntry;
	std::unique_ptr<SQLite::Statement> mStmtUpdateEntryTime;
	std::unique_ptr<SQLite::Statement> mStmtRenameEntry;
	std::unique_ptr<SQLite::Statement> mStmtRemoveEntry;

	std::unordered_map<std::string, std::vector<U8>> mFileMem; // memory cache, <uuid, data> map
};


#endif // DDD_CACHE_H
