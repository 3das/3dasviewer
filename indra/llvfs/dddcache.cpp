// 3D Avatar School 
// File based caching for VFS replacement. Currently used along with VFS

//#include "llviewerprecompiledheaders.h"

#define NOMINMAX  // prevent windows.h to define min/max macros. it breaks std::min/max

#include <algorithm>
#include <functional>
#include <vector>
#include <iostream>
#include <cassert>
#include <ctime>

#include "dddcache.h"
#include "SQLiteC++/Transaction.h"
#include "md5.h"


#ifndef DDD_STANDALONE
#include "llerror.h"
#endif

#include "fsutils.h"



namespace {

	// purging of files will be done if this limit is reached
	const S32 MAX_TOTAL_SIZE = 512*1024*1024; // 512 MB

	const int PURGE_CHECK_COUNTER = 100; // check if purging is needed only once in 100 times. in DDDCache::write()
	const double PURGE_RATIO = 0.2;     // purge 20% of entries
	
	const int TIMESTAMP_DIFF = 60 * 15; // 15 mins. In read, update timestamp only if diff > this value

	//const char* cache_dirname = "dddcache";
	const char* cache_entries_db_filename = "dddcache.sdb";


	bool cache_reset_needed(int rc)
	{
		return rc == SQLITE_CORRUPT || rc == SQLITE_EMPTY || rc == SQLITE_SCHEMA || rc == SQLITE_NOTADB;
	}

}




DDDCache::DDDCache(const std::string& cacheName, const std::string& parentDir) :
#ifdef DDD_USE_BOOST
 mLocalAPRFilePoolp(NULL),
#else
 mLocalAPRFilePoolp(new LLVolatileAPRPool()),
#endif
 mDBMutex(NULL),
 mPurgeCheckCounter(PURGE_CHECK_COUNTER)
{
#ifdef DDD_STANDALONE
	mCacheDirPath = (parentDir.empty() ? std::string(".") : parentDir) + PATH_SEPARATOR + cacheName;
	mEntriesDbPath = mCacheDirPath + PATH_SEPARATOR + cache_entries_db_filename;
#else
	mCacheDirPath = gDirUtilp->getExpandedFilename(LL_PATH_CACHE, cacheName);
	mEntriesDbPath = gDirUtilp->getExpandedFilename(LL_PATH_CACHE, cacheName, cache_entries_db_filename);
#endif

#ifdef DDD_USE_BOOST
	// set boost fs lib to accept UTF8 paths, even on Windows
	//std::locale path_locale(std::locale(), new boost::filesystem::detail::utf8_codecvt_facet);
	std::locale path_locale = boost::locale::generator().generate("");
	boost::filesystem::path::imbue(path_locale);
#endif

	dbInit(true);
}



DDDCache::~DDDCache()
{
	dbClose(); // explicitly destroy sqlite objects 

#ifndef DDD_USE_BOOST
	delete mLocalAPRFilePoolp;
	mLocalAPRFilePoolp = NULL;
#endif
}



S32 DDDCache::getSize(const LLUUID& uuid)
{
	return dbGetSize(uuid);
}



BOOL DDDCache::exists(const LLUUID& uuid)
{
	return getSize(uuid) != -1;
}



S32 DDDCache::read(const LLUUID& uuid, LLAssetType::EType assetType, U8* data, S32 location, S32 size)
{
	try
	{
		LLMutexLock lock(&mDBMutex);

		//llinfos << "Reading entry with UUID: " << uuid.asString() << llendl;

		std::string uuidStr = uuid.asString();
		const auto it = mFileMem.find(uuidStr);
		if (it != mFileMem.end())
		{
			return readMemData(it->second, data, location, size);
		}


		SQLite::Transaction transact(*mDB.get());

		CacheEntry entry;
		if (!dbFindEntry(uuid, entry))
		{
			llwarns << "Entry not found" << llendl;
			return -1;
		}

		if (entry.mAssetType != assetType)
		{
			llwarns << "Asset type mismatch" << llendl;
			return -1;
		}

		// we're called inside loop which terminates when nbr of read bytes is 0
		// so just skip unnecessary actual file reading and db updating when we know that there is no more data
		S32 nbytes = 0;
		if (location < entry.mFileSize)
		{
			// read the data from the UUID based cached file
			//nbytes = fsutils::read(getDataFilePath(uuid), data, location, size, getLocalAPRFilePool());

			// update last read timestamp
			U32 curTime = static_cast<U32>(std::time(NULL));
			if (curTime - entry.mTime > TIMESTAMP_DIFF) {
				entry.mTime = curTime;
				dbUpdateEntryTime(entry);
			}

			auto& vect = mFileMem[uuidStr];
			vect.resize(entry.mFileSize);
			fsutils::read(getDataFilePath(uuid), vect.data(), 0, entry.mFileSize, getLocalAPRFilePool());

			nbytes = readMemData(vect, data, location, size);
		}

		transact.commit();

		return nbytes;
	}
	catch (SQLite::Exception& e)
	{
		llerrs << "exception: " << e.what() << llendl;
		return -1;
	}
}



S32 DDDCache::write(const LLUUID& uuid, LLAssetType::EType assetType, const U8* data, S32 location, S32 size)
{
	S32 nbytes = -1;

	try
	{
		LLMutexLock lock(&mDBMutex);
		
		SQLite::Transaction transact(*mDB.get());

		//llinfos << "Writing entry with UUID: " << uuid.asString() << llendl;


		CacheEntry entry;
		dbFindEntry(uuid, entry);

		entry.mUUID = uuid;
		entry.mFileSize = (location == -1) ? entry.mFileSize + size : std::max(entry.mFileSize, location + size); // -1 == append
		entry.mAssetType = assetType;
		entry.mTime = static_cast<U32>(std::time(NULL));
	
		std::string dataFilePath = getDataFilePath(uuid);
		nbytes = fsutils::write(dataFilePath, const_cast<U8*>(data), location, size, getLocalAPRFilePool());

		if (nbytes != size)
		{
			llwarns << "Incorrect number of bytes written to file" << llendl;
			nbytes = -1;
		}
		//else
		//{
		//	//transact.commit();
		//	llinfos << "Success" << llendl;
		//}

		// calculate hash
		MD5 ctx;
		if (location == -1) // in case of append, read the whole data from file
		{
			// or better: write in memory (vector). maybe read will free it?
			// read can detect end of file (see cur notes, location + size == filesize)

			std::vector<U8> fileVec(entry.mFileSize);
			S32 readBytes = fsutils::read(dataFilePath, fileVec.data(), 0, entry.mFileSize, getLocalAPRFilePool());
			if (readBytes != entry.mFileSize)
			{
				llwarns << "In hash calculation: Incorrect number of bytes read from file" << llendl;
			}
			ctx.update(fileVec.data(), fileVec.size());
		}
		else
		{
			ctx.update(data, size);
		}
		
		ctx.finalize();
		entry.mHash = ctx.hexdigest();


		if (entry.mID == -1)
		{
			dbInsertEntry(entry);
		}
		else
		{
			dbUpdateEntry(entry);
		}
			
		if (--mPurgeCheckCounter <= 0)
		{
			mPurgeCheckCounter = PURGE_CHECK_COUNTER; // reset counter
			purgeFilesIfNeeded();
		}

		transact.commit();
	}
	catch (SQLite::Exception& e)
	{
		llerrs << "exception: " << e.what() << llendl;
		//return -1;
	}

	return nbytes;
}



void DDDCache::renameFile(const LLUUID& uuid, LLAssetType::EType assetType, const LLUUID& uuidNew, LLAssetType::EType assetTypeNew)
{
	llinfos << "Renaming entry with UUID: " << uuid.asString()
			<< " to UUID: " << uuidNew.asString() << llendl;

	dbRenameEntry(uuid, assetType, uuidNew, assetTypeNew);
	
	fsutils::rename(getDataFilePath(uuid), getDataFilePath(uuidNew));
}



void DDDCache::removeFile(const LLUUID& uuid, LLAssetType::EType assetType)
{
	llinfos << "Removing entry with UUID: " << uuid.asString() << " asset type: " << assetType << llendl;

	dbRemoveEntry(uuid, assetType);
	
	fsutils::remove(getDataFilePath(uuid));
}



void DDDCache::purgeFilesIfNeeded()
{
	// this method is called inside transaction

	S64 totalSize = mDB->execAndGet("SELECT SUM(file_size) FROM Entry;");
	if (totalSize < MAX_TOTAL_SIZE)
	{
		return; // we're fine
	}

	llinfos << "Purging files" << llendl;

	S32 count = mDB->execAndGet("SELECT COUNT(*) FROM Entry;");
	SQLite::Statement selStmt(*mDB.get(), "SELECT uuid, asset_type FROM Entry ORDER BY time DESC LIMIT ?");
	selStmt.bind(1, (int)(count * PURGE_RATIO));

	while(selStmt.executeStep())
	{
		removeFile(LLUUID(selStmt.getColumn(0).getText()), LLAssetType::EType(int(selStmt.getColumn(1))) );
	}
}



void DDDCache::resetCache()
{
	LLMutexLock lock(&mDBMutex);

	llinfos << "Resetting cache. (Re)creating dir structure" << llendl;

	const char* subdirs = "0123456789abcdef";
	std::string sep = fsutils::path_separator();

	fsutils::mkdir(mCacheDirPath); // create if it doesn't exist

	for (S32 i = 0; i < 16; i++)
	{
		std::string dirname = mCacheDirPath + sep + subdirs[i];
		llinfos << "Deleting directory recursively: " << dirname << llendl;
		fsutils::rmdir(dirname);
		fsutils::mkdir(dirname);
	}

	fsutils::remove(mEntriesDbPath);
}



std::string DDDCache::getDataFilePath(const LLUUID& uuid) const
{
	std::string idstr = uuid.asString();
	std::string sep = fsutils::path_separator();

	return mCacheDirPath + sep + idstr[0] + sep + idstr + ".dat";
}



S32 DDDCache::readMemData(const std::vector<U8>& vect, U8* data, S32 location, S32 size) const
{
	std::vector<U8>::const_iterator beginIt = vect.begin() + location;
	std::vector<U8>::const_iterator endIt = std::min(beginIt + size, vect.end());
	std::copy(beginIt, endIt, data);

	//llinfos << "Read from memo. Size: " << (endIt - beginIt) << " UUID: " << uuidStr << llendl;

	return endIt - beginIt;
};



// SQLite related methods -------------------

bool DDDCache::dbInit(bool retryOnce)
{
	try
	{
		LLMutexLock lock(&mDBMutex);
		if (mDB)
		{
			dbClose();
		}

		if (!fsutils::exists(mEntriesDbPath))
		{
			resetCache();
		}

		llinfos << "Initializing SQLite3 database..." << llendl;
		mDB.reset(new SQLite::Database(mEntriesDbPath.c_str(), SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE));

		mDB->exec("PRAGMA journal_mode = WAL;");
		mDB->exec("PRAGMA cache_size = 16000;");
		mDB->exec("PRAGMA synchronous = NORMAL;");
		//mDB->exec("PRAGMA synchronous = OFF;");
		

		if (!mDB->tableExists("Entry"))
		{
			//SQLite::Transaction transact(*mDB.get());

			const char* sqlCreateTable = "CREATE TABLE IF NOT EXISTS Entry ("
										 "id INTEGER NOT NULL PRIMARY KEY, "
										 "uuid TEXT NOT NULL COLLATE NOCASE, "
										 "hash TEXT NOT NULL COLLATE NOCASE, "
										 "file_size INTEGER NOT NULL, "
										 "asset_type INTEGER NOT NULL, "
										 "time INTEGER NOT NULL);";
			mDB->exec(sqlCreateTable);

			mDB->exec("CREATE INDEX IF NOT EXISTS entry_idx_id ON Entry (id);");
			mDB->exec("CREATE INDEX IF NOT EXISTS entry_idx_uuid ON Entry (uuid);");
			mDB->exec("CREATE INDEX IF NOT EXISTS entry_idx_time ON Entry (time);");
			
			//transact.commit();
		}

		dbPrepareStmts();
	}
	catch (SQLite::Exception& e)
	{
		if (cache_reset_needed(e.error_code()))
		{
			resetCache();
		}

		if (retryOnce)
		{
			return dbInit(false); // no more retries
		}
		else
		{
			llerrs << "exception: " << e.what() << llendl;
			return false;
		}
	}

	return true;
}



bool DDDCache::dbPrepareStmts()
{
	LLMutexLock lock(&mDBMutex);

	mStmtGetSize.reset(new SQLite::Statement(*mDB.get(), "SELECT file_size FROM Entry WHERE uuid = :uuid"));

	mStmtFindEntry.reset(new SQLite::Statement(*mDB.get(), "SELECT * FROM Entry WHERE uuid = :uuid"));
	
	mStmtInsertEntry.reset(new SQLite::Statement(*mDB.get(),
		"INSERT INTO Entry(uuid, hash, file_size, asset_type, time) VALUES(:uuid, :hash, :fsize, :atype, :time)"));
	
	mStmtUpdateEntry.reset(new SQLite::Statement(*mDB.get(),
		"UPDATE Entry SET uuid = :uuid, hash = :hash, file_size = :fsize, asset_type = :atype, time = :time WHERE id = :id"));
	
	mStmtUpdateEntryTime.reset(new SQLite::Statement(*mDB.get(),
		"UPDATE Entry SET time = :time WHERE id = :id"));

	mStmtRenameEntry.reset(new SQLite::Statement(*mDB.get(),
		"UPDATE Entry SET uuid = :newuuid, asset_type = :newatype WHERE uuid = :uuid AND asset_type = :atype"));

	mStmtRemoveEntry.reset(new SQLite::Statement(*mDB.get(),
		"DELETE FROM Entry WHERE uuid = :uuid AND asset_type = :atype"));

	return true;
}



S32 DDDCache::dbGetSize(const LLUUID& uuid)
{
	S32 size = -1;
	try
	{
		LLMutexLock lock(&mDBMutex);

		mStmtGetSize->bind(":uuid", uuid.asString());
		
		if (mStmtGetSize->executeStep())
		{
			size = mStmtGetSize->getColumn(0);
		}
		mStmtGetSize->reset();
	}
	catch (SQLite::Exception& e)
	{
		llerrs << "exception: " << e.what() << llendl;
		size = -1;
	}

	return size;
}



bool DDDCache::dbFindEntry(const LLUUID& uuid, CacheEntry& entry)
{
	try
	{
		LLMutexLock lock(&mDBMutex);
		mStmtFindEntry->bind(":uuid", uuid.asString());

		if (mStmtFindEntry->executeStep())
		{
			//uuid, hash, file_size, asset_type, time
			entry.mID = mStmtFindEntry->getColumn(0);
			entry.mUUID.set(mStmtFindEntry->getColumn(1).getText());
			entry.mHash = mStmtFindEntry->getColumn(2);
			entry.mFileSize = mStmtFindEntry->getColumn(3);
			entry.mAssetType = LLAssetType::EType((int)mStmtFindEntry->getColumn(4));
			entry.mTime = (U32) mStmtFindEntry->getColumn(5).getInt64();
		}
		else
		{
			entry.mID = -1;
		}
		mStmtFindEntry->reset();
	}
	catch (SQLite::Exception& e)
	{
		entry.mID = -1;
		llerrs << "exception: " << e.what() << llendl;
	}
	
	return entry.mID != -1;
}



bool DDDCache::dbUpdateEntryTime(const CacheEntry& entry)
{
	try
	{
		LLMutexLock lock(&mDBMutex);
		mStmtUpdateEntryTime->bind(":time", (sqlite3_int64)entry.mTime);
		mStmtUpdateEntryTime->bind(":id", entry.mID);

		mStmtUpdateEntryTime->executeStep();

		mStmtUpdateEntryTime->reset();
	}
	catch (SQLite::Exception& e)
	{
		llerrs << "exception: " << e.what() << llendl;
		return false;
	}

	return true;
}



void DDDCache::dbInsertEntry(CacheEntry& entry)
{
	try
	{
		LLMutexLock lock(&mDBMutex);
		mStmtInsertEntry->bind(":uuid", entry.mUUID.asString());
		mStmtInsertEntry->bind(":hash", entry.mHash);
		mStmtInsertEntry->bind(":fsize", entry.mFileSize);
		mStmtInsertEntry->bind(":atype", entry.mAssetType);
		mStmtInsertEntry->bind(":time", (sqlite3_int64)entry.mTime);

		mStmtInsertEntry->executeStep();

		entry.mID = mDB->getLastInsertRowid();
		if (entry.mID == 0) // no insert
		{
			entry.mID = -1;
		}

		mStmtInsertEntry->reset();
	}
	catch (SQLite::Exception& e)
	{
		llerrs << "exception: " << e.what() << llendl;
	}
}



void DDDCache::dbUpdateEntry(const CacheEntry& entry)
{
	try
	{
		LLMutexLock lock(&mDBMutex);
		mStmtUpdateEntry->bind(":uuid", entry.mUUID.asString());
		mStmtUpdateEntry->bind(":hash", entry.mHash);
		mStmtUpdateEntry->bind(":fsize", entry.mFileSize);
		mStmtUpdateEntry->bind(":atype", entry.mAssetType);
		mStmtUpdateEntry->bind(":time", (sqlite3_int64)entry.mTime);
		mStmtUpdateEntry->bind(":id", entry.mID);

		mStmtUpdateEntry->executeStep();

		mStmtUpdateEntry->reset();
	}
	catch (SQLite::Exception& e)
	{
		llerrs << "exception: " << e.what() << llendl;
	}
}



void DDDCache::dbRenameEntry(const LLUUID& uuid, LLAssetType::EType assetType, const LLUUID& uuidNew, LLAssetType::EType assetTypeNew)
{
	try
	{
		LLMutexLock lock(&mDBMutex);
		mStmtRenameEntry->bind(":uuid", uuid.asString());
		mStmtRenameEntry->bind(":atype", assetType);
		mStmtRenameEntry->bind(":newuuid", uuidNew.asString());
		mStmtRenameEntry->bind(":newatype", assetTypeNew);

		mStmtRenameEntry->executeStep();

		mStmtRenameEntry->reset();
	}
	catch (SQLite::Exception& e)
	{
		llerrs << "exception: " << e.what() << llendl;
	}
}



void DDDCache::dbRemoveEntry(const LLUUID& uuid, LLAssetType::EType assetType)
{
	try
	{
		LLMutexLock lock(&mDBMutex);
		mStmtRemoveEntry->bind(":uuid", uuid.asString());
		mStmtRemoveEntry->bind(":atype", assetType);

		mStmtRemoveEntry->executeStep();

		mStmtRemoveEntry->reset();
	}
	catch (SQLite::Exception& e)
	{
		llerrs << "exception: " << e.what() << llendl;
	}
}



void DDDCache::dbClose()
{
	// delete statements first 
	delete mStmtGetSize.release();
	delete mStmtFindEntry.release();
	delete mStmtInsertEntry.release();
	delete mStmtUpdateEntry.release();
	delete mStmtUpdateEntryTime.release();
	delete mStmtRenameEntry.release();
	delete mStmtRemoveEntry.release();

	delete mDB.release();
}

