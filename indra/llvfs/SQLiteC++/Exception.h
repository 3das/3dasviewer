/**
 * @file  Exception.h
 * @brief Encapsulation of the error message from SQLite3 on a std::runtime_error.
 *
 * Copyright (c) 2012 Sebastien Rombauts (sebastien.rombauts@gmail.com)
 *
 * Distributed under the MIT License (MIT) (See accompanying file LICENSE.txt
 * or copy at http://opensource.org/licenses/MIT)
 */
// Modified by Zurab Khetsuriani: error code is passed through Exception
#pragma once

#include <stdexcept>

#ifdef _WIN32
#pragma warning(disable:4290) // Disable warning C4290: C++ exception specification ignored except to indicate a function is not __declspec(nothrow)
#endif

namespace SQLite
{

/**
 * @brief Encapsulation of the error message from SQLite3, based on std::runtime_error.
 */
class Exception : public std::runtime_error
{
public:
    Exception(const std::string& aErrorMessage, int rc = -1) :
        std::runtime_error(aErrorMessage), m_rc(rc)
    {
    }

	int error_code() const  { return m_rc; }

private:
	int m_rc; 
};


}  // namespace SQLite
