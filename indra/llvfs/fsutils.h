// file system utils - abstraction for boost fs vs apache fs and LLFile lib usages
#ifndef DDD_FSUTILS_H
#define DDD_FSUTILS_H

#include "dddcache.h"

#ifdef DDD_STANDALONE
#include "slmisc.h"
#endif

#ifdef DDD_USE_BOOST
#include <boost/locale.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/fstream.hpp>
//#include <boost/filesystem/detail/utf8_codecvt_facet.hpp>
#else
#include "llapr.h"
#include "lldir.h"
#endif


namespace fsutils {



inline S32 read(const std::string& file, U8* data, S32 location, S32 size, LLVolatileAPRPool* pool = NULL)
{
#ifdef DDD_USE_BOOST
	boost::filesystem::ifstream infile(file, std::ios_base::in | std::ios_base::binary);
	infile.seekg(location, std::ios::beg);
	infile.read((char*) data, size);
	return (S32)infile.gcount();
#else
	return LLAPRFile::readEx(file, data, location, size, pool);
#endif
}



inline S32 write(const std::string& file, const U8* data, S32 location, S32 size, LLVolatileAPRPool* pool = NULL)
{
#ifdef DDD_USE_BOOST
	
	int mode = std::ios::out | std::ios::binary;
	
	if (boost::filesystem::exists(file))
	{
		mode |= std::ios::in;  // needed for correct seeking
	}

	if (location == -1)
	{
		mode |= std::ios_base::app;  // append
	}

	boost::filesystem::ofstream outfile(file, mode);
	if (location > 0)
	{
		outfile.seekp(location, std::ios::beg);
	}
	std::streamoff prevPos = outfile.tellp();
	outfile.write((char*) data, size);

	return (S32) (outfile.tellp() - prevPos);

#else
	// writeEx takes non-const void* data, but in fact it uses the data as const
	return LLAPRFile::writeEx(file, const_cast<U8*>(data), location, size, pool);
#endif
}



inline void mkdir(const std::string& dir)
{
#ifdef DDD_USE_BOOST
	boost::filesystem::create_directory(dir);
#else
	LLFile::mkdir(dir);
#endif
}



inline void rmdir(const std::string& dir)
{
#ifdef DDD_USE_BOOST
	boost::filesystem::remove_all(dir);
#else
	gDirUtilp->deleteFilesInDir(dir, "*");
	LLFile::rmdir(dir);
#endif
}



inline void remove(const std::string& file)
{
#ifdef DDD_USE_BOOST
	boost::filesystem::remove(file);
#else
	LLFile::remove(file);
#endif
}



inline void rename(const std::string& src, const std::string& dest)
{
#ifdef DDD_USE_BOOST
	boost::filesystem::rename(src, dest);
#else
	LLFile::rename(src, dest);
#endif
}



inline bool exists(const std::string& file)
{
#ifdef DDD_USE_BOOST
	return boost::filesystem::exists(file);
#else
	return gDirUtilp->fileExists(file);
#endif
}



inline std::string path_separator()
{
#ifdef DDD_USE_BOOST
	return PATH_SEPARATOR;
#else
	return gDirUtilp->getDirDelimiter();
#endif
}



} // fsutils



#endif // DDD_FSUTILS_H
