// 3D Avatar School 
// dddinit.cpp
// initialisation routines
// 


#include "llviewerprecompiledheaders.h"
#include "dddlogsender.h"
#include "dddstatssender.h"
#include "../newview/llagent.h"
#include "dddsettings.h"
#include "llappviewer.h"

#include "dddshortestconnection.h"
#include "dddnetworktestpump.h"
#include "dddusbbusenum.h"
#include "ddddevicedriverenumeration.h"

// initialize 3D Avatar School subsystem
int DDDInitialize()
{
	std::string slUser;
	DDDStatsSender::sendMachineStats(slUser);

	DDDSettings::instance().statsSent=TRUE;
	if (DDDSettings::instance().dieAfterStatsSend() && DDDSettings::instance().dieCallbackCalled)
	{
		LLAppViewer::instance()->forceQuit();
	}

	DDDLogSender::sendLog("SecondLife.old");


	DDDUsbBusEnum::instance().logUSBDevices();

	DDDDeviceDriverEnumeration::instance().report();

	// students get mic on all the time, as teachers have the button, let's not embarass them...
	if (!DDDSettings::instance().isTeacher())
		LLAgent::pressMicrophone(LLSD());


	DDDNetworkTestPump::instance().startup();


	return true;
}

