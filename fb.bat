rm -rf build-vc100
@echo --------- configure ----------
call autobuild configure -c ReleaseOS -- --verbose --debug -DLL_TESTS:BOOL=FALSE -DPACKAGE:BOOL=TRUE -DFMOD:BOOL=TRUE -DOPENAL:BOOL=TRUE > configure.out 2>&1
@echo ----- configure complete -----
IF ERRORLEVEL 1 goto conffail
@echo ----------- build ------------
call autobuild build -c ReleaseOS --verbose --debug --no-configure > build.out 2>&1
@echo --------- build done ---------
@echo ----------- errors -----------
grep -i error build.out
goto end
:conffail
@echo ----------configure failed ---------
grep -i error configure.out
@echo ----------configure failed ---------
:end
